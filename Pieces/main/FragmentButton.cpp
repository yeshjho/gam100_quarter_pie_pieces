#include "FragmentButton.h"
#include "FragmentsWidget.h"
#include "constants.h"
#include "UI.h"

using namespace doodle;
using namespace std;

FragmentButton::FragmentButton(const Position pos, float w, float h, char fragletter, int quantt, UIObject* parent)
	:ButtonObject(pos,w,h,parent),letter(fragletter),quantity(quantt)
{
	fragment.push_back(letter);
}

FragmentButton::~FragmentButton()
{
	
}

void FragmentButton::onClicked()
{
	ButtonObject::onClicked();
	if (quantity <= 0)
		return;

	FragmentsWidget* p_fragments_widget = static_cast<FragmentsWidget*>(pParent);
	UI* p_ui = static_cast<UI*>(p_fragments_widget->getParent());
	quantity--;
	p_ui->addToBuffer(letter);
}

void FragmentButton::onReleased()
{
	ButtonObject::onReleased();
}

void FragmentButton::update()
{
	ButtonObject::update();
}

void FragmentButton::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text(fragment, position.x+5.f, position.y);
	draw_text(to_string(quantity), position.x + width / 2.f, position.y);
	pop_settings();
}

void FragmentButton::addQuantity(int n)
{
	quantity += n;
}

void FragmentButton::setQuantityTo(int n)
{
	if (n < 0)
		quantity = 0;
	else
		quantity = n;
}
