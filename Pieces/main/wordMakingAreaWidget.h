/*
	wordMakingAreaWidget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Widget.h"
#include "ButtonObject.h"
#include "DraggableWordObject.h"
using namespace std;

class wordMakingAreaWidget : public Widget
{
public:
	wordMakingAreaWidget(const Position pos, float w, float h, UIObject* parent);
	void combine();
	void decompose();
	virtual void onClicked()override;
	virtual void onReleased()override;
	void onDragObjectDropped(DragObject* obj);
	virtual void update()override;
	virtual void draw()override;
	void addToBuffer(char letter);
	void reset();
private:
	unordered_map<char, vector<string>> possible_keywords = { {'a',{"armor"}},{'c',{"crabdamage", "crabhealth"}},
															  {'e',{"else"}},{'f',{"frogdamage", "frogjump", "froghealth","for",}},{'g',{"giveitem"}},
															  {'i',{"if"}},
															  {'p',{"playerjump", "playerhealth", "playerspeed"}},{'s',{"sword"}},
															  {'w',{"while"}},{'x',{"xdamage", "xjump", "xhealth","xyzdamage", "xyzjump", "xyzhealth","xsword","xarmor"}},{'0',{"0"}},
															  {'1',{"1"}},{'2',{"2"}},{'3',{"3"}},{'4',{"4"}},{'5',{"5"}},
															  {'6',{"6"}},{'7',{"7"}},{'8',{"8"}},{'9',{"9"}},{'!',{"!","!="}},
	                                                          {'&',{"&"}},{'|',{"|"}},
															  {'+',{"+","+="}},{'-',{"-","-="}},{'%',{"%","%="}},{'/',{"/","/="}},
														      {'*',{"*","*="}},{'=',{"=","=="}},{'<',{"<","<="}},{'>',{">",">="}},
															  {'(',{"("}},{')',{")"}}
	};
	string buffer;
	vector<ButtonObject*> buttons;
	float one_word_height;

	void emptyBuffer();
	void drawPossibleWords();
};

