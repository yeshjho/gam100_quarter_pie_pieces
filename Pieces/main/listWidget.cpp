#include "listWidget.h"

listWidget::listWidget(const Position pos, float w, float h, UIObject* parent)
	:Widget(pos,w,h,parent)
{

}

listWidget::~listWidget()
{
}

void listWidget::addObjectToList(UIObject* obj)
{
	DragObject* dragObj = static_cast<DragObject*>(obj);
	dragObj->setParent(this);
	listedObjects.push_back(obj);
}

void listWidget::removeObjectFromList(UIObject* obj)
{
	listedObjects.remove(obj);
}

void listWidget::alignObjects()
{
	float accumulated_yPos = 0.f;
	for (UIObject* obj : listedObjects)
	{
		float objHeight = obj->getHeight();
		float newPosY = this->position.y + accumulated_yPos;
		obj->setPosXY(position.x,newPosY);
		accumulated_yPos += objHeight;
	}
}

void listWidget::resize(float w, float h)
{
	width = w;
	height = h;
}

