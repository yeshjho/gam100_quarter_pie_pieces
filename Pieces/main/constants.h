/*
	constants.h

	GAM100, Fall 2019

	JoonHo Hwang wrote some of them
	Doyoon Kim wrote the most
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

constexpr char PLAYER = 0x1;
constexpr char FROG = 0x2;
constexpr char CRAB = 0x4;
constexpr char PORTAL = 0x8;
constexpr char BULLET = 0x10;
constexpr char MIDDLE_BOSS = 0x20;
constexpr char FINAL_BOSS = 0x40;
constexpr char CHEST = 0x70;

constexpr float GRAVITY = -3.f;

constexpr int MAP_WIDTH = 45;
constexpr int MAP_HEIGHT = 25;
constexpr int BLOCK_WIDTH = 30;
constexpr int BLOCK_HEIGHT = 30;
constexpr int HITBOX_WIDTH = 40;
constexpr int HITBOX_HEIGHT = 40;

constexpr int DRAGGABLE_WORD_OBJECT = 0x1;
constexpr int SINGLE_CODE_PIECE = 0x2;
constexpr int CONDITIONAL_PIECE = 0x4;

constexpr unsigned char BOSS_ROOM_NUMBER = 8;

enum class DIRECTION{Up,Right,Left,Down};

constexpr unsigned short LOGO_DISPLAY_FRAME = 144 * 2;

constexpr const char* GAME_TITLE = "Pieces";

constexpr float UITextSize = 15.f;

constexpr int initialFragmentQuantity = 0;

constexpr float singleCodePieceHeight = 45.f;

int constexpr MaxSingleCodePiecePerLine = 8;