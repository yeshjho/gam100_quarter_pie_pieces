/*
	CodePieceIF.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ConditionalPieces.h"
#include "ConditionExpressionWidget.h"
#include "CodeBodyWidget.h"

class CodePieceIf : public ConditionalPieces
{
public:
	CodePieceIf(Position pos, float w, float h, UIObject* parent, int type, const CodePiece& rCodeType, const string& nWord);
	virtual ~CodePieceIf()override;
	virtual void RegisterWidgetsTo(listWidget* codingAreaWidget)override;
	virtual void DeRegisterWidgetsFrom(listWidget* codingAreaWidget)override;
	virtual void draw()override;
	virtual float getHeight()const override;
	virtual void alignObjects()override;
	virtual void resize(float w, float h)override;
	virtual void update() override;
	virtual vector<string> getCodePiecesToWords() override;
	virtual vector<CodePiece> getInterpreterCodePieces()override;
private:
	ConditionExpressionWidget conditionExpressionWidget;
};

