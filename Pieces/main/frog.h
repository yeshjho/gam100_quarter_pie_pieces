/*
	frog.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim designed the structure
	Junhyuk Cha implemented

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

enum class FrogState
{
	DEAD, ALIVE
};

class Frog : public Object
{
public:
	Frog(const Vector& pos);
	Frog(const Vector& pos, const Vector& spd);
	~Frog() override;
	void update() override;
	void draw() override;
	void move() override;
	void addForce(const Vector& force) override;
	void between_blocks() override;
	void onDead() override;

	void check_is_cooldown();
	void checkCollisionWithPlayer() override;

	void cooldown_damage();
	void set_current_state(FrogState State);
	FrogState get_current_state();
	void set_health(int Health);
	int get_health();
	void set_damage(float Damage);

	void setJumpValue(float jumpValue);
private:
	float mass = 10.f;
	float width = 20.f;
	float height = 20.f;
	float jump_value = 70.f;
	bool frog_jump = false;
	bool is_on_ground = false;
	float moving_left_right = 20.f;
	float check_current_cooldown = 0;
	float check_current_damage_cooldown = 0;
	bool can_give_damage = true;
	FrogState state = FrogState::ALIVE;
	int health = 2;
	float damage = 1.5;
};

