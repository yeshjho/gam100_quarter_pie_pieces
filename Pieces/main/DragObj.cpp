#include "DragObj.h"
#include "global.h"

using doodle::get_mouse_x;
using doodle::get_mouse_y;

DragObject::DragObject(const Position pos, float w, float h, UIObject* parent,int nType)
	:UIObject(pos,w,h),pParent(parent),type(nType)
{
}

DragObject::~DragObject()
{
}

void DragObject::onClicked()
{
	isClicked = true;
}

void DragObject::onReleased()
{
	isClicked = false;
}

void DragObject::update()
{
	if (isClicked)
	{
		position.x = static_cast<float>(get_mouse_x()-width/2.f);
		position.y = static_cast<float>(get_mouse_y()-height/2.f);
	}
}

int DragObject::getType()const
{
	return type;
}

UIObject* DragObject::getParent()
{
	return pParent;
}

void DragObject::setParent(UIObject* parent)
{
	pParent = parent;
}
