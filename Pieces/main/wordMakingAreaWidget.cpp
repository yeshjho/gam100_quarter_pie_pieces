﻿#include "wordMakingAreaWidget.h"
#include "UI.h"
#include "combineButton.h"
#include "DecomposeButton.h"
#include "doodle/doodle.hpp"
#include "global.h"

using namespace doodle;

wordMakingAreaWidget::wordMakingAreaWidget(const Position pos, float w, float h, UIObject* parent)
	:Widget(pos,w,h,parent),one_word_height(h/5.f)
{
	UIObject* thisAdress = static_cast<UIObject*>(this);

	float button_w = w / 4.f;
	float button_h = h / 8.f;

	//create combine and decompose buttons
	CombineButton* combineButton = new CombineButton({ pos.x,pos.y }, button_w, button_h, thisAdress);
	DecomposeButton* decomposeButton = new DecomposeButton({ pos.x + w / 2.f + w / 4.f,pos.y }, button_w, button_h, thisAdress);
	
	buttons.push_back(combineButton);
	buttons.push_back(decomposeButton);
	ui.registerUIButtonObject(combineButton);
	ui.registerUIButtonObject(decomposeButton);
}


void wordMakingAreaWidget::combine()
{
	int bufferSize = (int)buffer.size();
	UI* p_ui = static_cast<UI*>(pParent);
	const vector<string>& possibleKeywords = possible_keywords[buffer[0]];

	for (string keyword : possibleKeywords)
	{
		if (keyword.compare(buffer) == 0)
		{
			p_ui->addToHoldingWords(buffer);
			buffer.clear();
			return;
		}
	}

	for (int i = 0; i < bufferSize; i++)
	{
		p_ui->fragmentsTable[buffer[i]]->addQuantity(1);
	}
	buffer.resize(0);

}

void wordMakingAreaWidget::decompose()
{
	emptyBuffer();
}

void wordMakingAreaWidget::onClicked()
{
}

void wordMakingAreaWidget::onReleased()
{
}

void wordMakingAreaWidget::onDragObjectDropped(DragObject* obj)
{
	int type = obj->getType();
	if (type != DRAGGABLE_WORD_OBJECT)
	{
		obj->setIsClicked(false);
		return;
	}
	DraggableWordObject* wordObj = static_cast<DraggableWordObject*>(obj);
	HoldingWordsWidget* parent = static_cast<HoldingWordsWidget*>(wordObj->getParent());
	parent->removeObjectFromList(obj);
	ui.removeUIDragObject(obj);
	emptyBuffer();
	buffer = wordObj->getWord();

	delete wordObj;
}

void wordMakingAreaWidget::update()
{
	for (ButtonObject* button : buttons)
	{
		button->update();
	}
}

void wordMakingAreaWidget::draw()
{
	push_settings();
	set_font_size(20.f);
	draw_rectangle(position.x, position.y, width, height);
	draw_text(buffer, position.x, position.y + height - one_word_height);
	pop_settings();
	drawPossibleWords();
	//draw buttons.
	for (ButtonObject* button : buttons)
	{
		button->draw();
	}
}

void wordMakingAreaWidget::addToBuffer(char letter)
{
	buffer.push_back(letter);
}

void wordMakingAreaWidget::reset()
{
	buffer.clear();
}


void wordMakingAreaWidget::emptyBuffer()
{
	int bufferSize = (int)buffer.size();
	UI* p_ui = static_cast<UI*>(pParent);

	for (int i = 0; i < bufferSize; i++)
	{
		p_ui->fragmentsTable[buffer[i]]->addQuantity(1);
	}
	buffer.resize(0);
}

void wordMakingAreaWidget::drawPossibleWords()
{
	const vector<string>& possibleWords = possible_keywords[buffer[0]];
	vector<string> matchingPossibleWords;

	for (const string& word : possibleWords)
	{
		int compareResult = buffer.compare(word);
		if (compareResult <= 0)
			matchingPossibleWords.push_back(word);
	}

	//space for buffer word
	float accumulated_word_height = 2.f*one_word_height;
	for (const string& word : matchingPossibleWords)
	{
		float posY = position.y + height - accumulated_word_height;
		push_settings();
		set_font_size(20.f);
		draw_rectangle(position.x, posY, width, one_word_height);
		draw_text(word, position.x, posY);
		pop_settings();
		accumulated_word_height += one_word_height;
	}
}
