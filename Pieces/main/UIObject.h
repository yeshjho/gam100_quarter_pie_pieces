/*
	UIObject.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "common.h"
#include "doodle/doodle.hpp"

class UIObject
{
public:
	UIObject(const Position pos, float w, float h);
	virtual ~UIObject() = 0;
	virtual void onClicked() = 0;
	virtual void onReleased() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
	bool checkIsMouseInTheBorder()const;
	bool getIsClicked() const;
	void setIsClicked(bool isclicked);
	virtual void resize(float w, float h);
	virtual void setPosXY(float n_x, float n_y);
	void setPosX(float n_x);
	void setPosY(float n_y);
	Position getPosXY()const;
	float getPosX()const;
	float getPosY()const;
	float getWidth()const;
	virtual float getHeight()const;
protected:
	bool isClicked = false;
	Position position;
	float width;
	float height;
};

