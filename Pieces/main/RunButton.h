/*
	RunButton.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ButtonObject.h"
class RunButton : public ButtonObject
{
public:
	RunButton(const Position pos, float w, float h, UIObject* parent);
	virtual ~RunButton()override;
	virtual void onClicked()override;
	virtual void onReleased() override;
	virtual void update() override;
	virtual void draw() override;
private:
};

