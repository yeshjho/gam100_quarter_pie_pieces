#include "Interpreter.h"

using std::string;
using std::unordered_map;
using std::function;
using std::get;

using namespace UserCodeInterpreter;


Interpreter::Interpreter(const unordered_map<string, Value*>& variableDict,
						 const unordered_map<string, function<void(const string&)>>& functionDict,
						 const CodePieceDeque& codePieces)
	: variableDictionary(variableDict), functionDictionary(functionDict),
	codePieceStream(codePieces), codePieceStreamSize(codePieces.size()) {}

void Interpreter::setCodePieceStream(const CodePieceDeque& codePieces)
{
	codePieceStream = codePieces;
}


CodePiece Interpreter::getCodePiece()
{
	if (codePieceIndex >= codePieceStreamSize)
	{
		throw "codePieceStream size overflow";
	}
	return codePieceStream.at(codePieceIndex++);
}

void Interpreter::ungetCodePiece()
{
	if (codePieceIndex == 0)
	{
		throw "codePieceStream size underflow";
	}
	codePieceIndex--;
}

double Interpreter::getNullDenotation(const CodePieceDeque& codePieces)
{
	switch (codePieces.at(evaluateCodePieceIndex).getType())
	{
		case ECodeType::CONSTANT:
			return get<double>(codePieces.at(evaluateCodePieceIndex).getValue());

		case ECodeType::VARIABLE:
			return get<double>(*variableDictionary.at(get<string>(codePieces.at(evaluateCodePieceIndex).getValue())));

		case ECodeType::NOT:
			evaluateCodePieceIndex++;
			return static_cast<double>(!static_cast<bool>(getNullDenotation(codePieces)));

		case ECodeType::ADD:
			evaluateCodePieceIndex++;
			return getNullDenotation(codePieces);

		case ECodeType::SUBTRACT:
			evaluateCodePieceIndex++;
			return -getNullDenotation(codePieces);

		case ECodeType::PARENTHESIS_OPEN:
		{
			CodePieceDeque expression{};
			size_t expressionIndex = evaluateCodePieceIndex + 1;
			CodePiece nextCodePiece = codePieces.at(expressionIndex);
			while (nextCodePiece.getType() != ECodeType::PARENTHESIS_CLOSE)
			{
				expression.push_back(nextCodePiece);
				nextCodePiece = codePieces.at(++expressionIndex);
			}

			evaluateCodePieceIndex += expression.size() + 1;
			
			Interpreter* expressionInterpreter = new Interpreter(variableDictionary, functionDictionary, {});
			const double to_return = expressionInterpreter->evaluate(expression);
			delete expressionInterpreter;
			return to_return;
		}

		default:
			throw "syntax error";
	}
}

double Interpreter::getLeftDenotation(const CodePieceDeque& codePieces, double lValue)
{
	const CodePiece currentCodePiece = codePieces.at(evaluateCodePieceIndex);
	evaluateCodePieceIndex++;
	switch (currentCodePiece.getType())
	{
		case ECodeType::ADD:
			return lValue + evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::SUBTRACT:
			return lValue - evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::MULTIPLY:
			return lValue * evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::DIVIDE:
			return lValue / evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::REMAINDER:
			return std::fmod(lValue, evaluate(codePieces, currentCodePiece.getLBP()));

		case ECodeType::AND:
			return static_cast<bool>(lValue) && static_cast<bool>(evaluate(codePieces, currentCodePiece.getLBP()));

		case ECodeType::OR:
			return static_cast<bool>(lValue) || static_cast<bool>(evaluate(codePieces, currentCodePiece.getLBP()));

		case ECodeType::EQUAL:
			return lValue == evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::NOT_EQUAL:
			return lValue != evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::GREATER:
			return lValue > evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::GREATER_EQUAL:
			return lValue >= evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::LESSER:
			return lValue < evaluate(codePieces, currentCodePiece.getLBP());

		case ECodeType::LESSER_EQUAL:
			return lValue <= evaluate(codePieces, currentCodePiece.getLBP());

		default:
			throw "syntax error";
	}
}

double Interpreter::evaluate(const CodePieceDeque& codePieces, unsigned char rightBindingPower, bool shouldResetIndex)
{
	if (shouldResetIndex)
	{
		evaluateCodePieceIndex = 0;
	}
	const size_t codeSize = codePieces.size();

	double lValue = getNullDenotation(codePieces);
	evaluateCodePieceIndex++;

	while (evaluateCodePieceIndex + 1 < codeSize && rightBindingPower < codePieces.at(evaluateCodePieceIndex).getLBP())
	{
		lValue = getLeftDenotation(codePieces, lValue);
	}
	return lValue;
}

CodePieceDeque Interpreter::getBlock()
{
	CodePieceDeque to_return{};
	
	size_t startCount = 0;
	CodePiece nextCodePiece = getCodePiece();
	while (true)
	{
		if (nextCodePiece.getType() == ECodeType::START)
		{
			startCount++;
		}
		else if (nextCodePiece.getType() == ECodeType::END)
		{
			if (startCount == 0)
			{
				break;
			}
			startCount--;
		}

		to_return.push_back(nextCodePiece);
		nextCodePiece = getCodePiece();
	}

	return to_return;
}

CodePieceDeque Interpreter::getUntilBoundary()
{
	CodePieceDeque to_return{};

	CodePiece nextCodePiece = getCodePiece();
	while (nextCodePiece.getType() != ECodeType::BOUNDARY)
	{
		to_return.push_back(nextCodePiece);
		nextCodePiece = getCodePiece();
	};

	return to_return;
}

CodePieceDeque Interpreter::getLine()
{
	CodePieceDeque to_return{};

	CodePiece nextCodePiece = getCodePiece();
	while (nextCodePiece.getType() != ECodeType::END_LINE && nextCodePiece.getType() != ECodeType::PROGRAM_END)
	{
		to_return.push_back(nextCodePiece);
		nextCodePiece = getCodePiece();
	};

	return to_return;
}

void Interpreter::ignoreLine()
{
	CodePiece nextCodePiece = getCodePiece();
	while (nextCodePiece.getType() != ECodeType::END_LINE && nextCodePiece.getType() != ECodeType::PROGRAM_END)
	{
		nextCodePiece = getCodePiece();
	}
}

bool Interpreter::interpret()
{
	/*
	GRAMMAR

	Program:
		Statement* PROGRAM_END

	Statement:
		LogicExpression END_LINE
		Assignment

		FUNCTION [CONSTANT VARIABLE] END_LINE

		START [IfStatement ForStatement WhileStatement] END

	IfStatement:
		IF LogicExpression BOUNDARY Statement* (END ELSE Statement*)?

	ForStatement:
		FOR Assignment? BOUNDARY LogicExpression BOUNDARY Assignment? BOUNDARY Statement*

	WhileStatement:
		WHILE LogicExpression BOUNDARY Statement*

	Assignment:
		VARIABLE [IS ADD_IS SUBTRACT_IS MULTIPLY_IS DIVIDE_IS REMAINDER_IS] LogicExpression END_LINE

	LogicExpression:
		LogicExpression [GREATER GREATER_EQUAL LESSER LESSER_EQUAL EQUAL NOT_EQUAL] LogicExpression
		LogicExpression [AND OR] LogicExpression

		Expression

	Expression:
		Expression [ADD SUBTRACT] Term

		Term

	Term:
		Term [MULTIPLY DIVIDE REMAINDER] Primary

		Primary

	Primary:
		PARENTHESIS_OPEN LogicExpression PARENTHESIS_CLOSE

		CONSTANT
		VARIABLE

		ADD CONSTANT
		SUBTRACT CONSTANT

		NOT Primary
	*/
	codePieceIndex = 0;

	try
	{
		CodePiece currentCodePiece = getCodePiece();
		while (currentCodePiece.getType() != ECodeType::PROGRAM_END)
		{
			switch (currentCodePiece.getType())
			{
			case ECodeType::START:
				switch (getCodePiece().getType())
				{
				case ECodeType::IF:
				{
					const CodePieceDeque condition = getUntilBoundary();

					CodePieceDeque ifBlock = getBlock();
					CodePieceDeque elseBlock{};
					if (getCodePiece().getType() == ECodeType::ELSE)
					{
						elseBlock = getBlock();
					}
					else
					{
						ungetCodePiece();
					}

					Interpreter* blockInterpreter = nullptr;
					if (!ifBlock.empty() && static_cast<bool>(evaluate(condition, 0, true)))
					{
						// or else it'll try to read after the last token
						ifBlock.push_back(ECodeType::PROGRAM_END);
						blockInterpreter = new Interpreter(variableDictionary, functionDictionary, ifBlock);
					}
					else if (!elseBlock.empty())
					{
						// or else it'll try to read after the last token
						elseBlock.push_back(ECodeType::PROGRAM_END);
						blockInterpreter = new Interpreter(variableDictionary, functionDictionary, elseBlock);
					}

					if (blockInterpreter)
					{
						blockInterpreter->interpret();
						delete blockInterpreter;
					}
					break;
				}

				case ECodeType::FOR:
				{
					CodePieceDeque initialization = getUntilBoundary();
					const CodePieceDeque condition = getUntilBoundary();
					CodePieceDeque afterthought = getUntilBoundary();
					CodePieceDeque body = getBlock();

					// or else it'll try to read after the last token
					initialization.push_back(ECodeType::PROGRAM_END);
					afterthought.push_back(ECodeType::PROGRAM_END);
					body.push_back(ECodeType::PROGRAM_END);

					Interpreter* initializationInterpreter = new Interpreter(variableDictionary, functionDictionary, initialization);
					initializationInterpreter->interpret();
					delete initializationInterpreter;

					Interpreter* bodyInterpreter = new Interpreter(variableDictionary, functionDictionary, body);
					Interpreter* afterthoughtInterpreter = new Interpreter(variableDictionary, functionDictionary, afterthought);
					unsigned char loopCount = 0;
					while (static_cast<bool>(evaluate(condition, 0, true)))
					{
						bodyInterpreter->interpret();
						afterthoughtInterpreter->interpret();
						if (++loopCount >= MAX_LOOP_COUNT)
						{
							throw "too many loops";
						}
					}
					delete bodyInterpreter;
					delete afterthoughtInterpreter;
					break;
				}

				case ECodeType::WHILE:
				{
					const CodePieceDeque condition = getUntilBoundary();
					CodePieceDeque body = getBlock();

					// or else it'll try to read after the last token
					body.push_back(ECodeType::PROGRAM_END);

					Interpreter* bodyInterpreter = new Interpreter(variableDictionary, functionDictionary, body);
					unsigned char loopCount = 0;
					while (static_cast<bool>(evaluate(condition, 0, true)))
					{
						bodyInterpreter->interpret();
						if (++loopCount >= MAX_LOOP_COUNT)
						{
							throw "too many loops";
						}
					}
					break;
				}

				default:
					throw "syntax error";
				}
				break;

			case ECodeType::VARIABLE:
			{
				const CodePiece nextCodePiece = getCodePiece();
				switch (nextCodePiece.getType())
				{
				case ECodeType::IS:
				{
					const CodePieceDeque rValue = getLine();
					*variableDictionary.at(get<string>(currentCodePiece.getValue())) = evaluate(rValue, 0, true);
					break;
				}

				case ECodeType::ADD_IS:
				{
					const CodePieceDeque rValue = getLine();
					*variableDictionary.at(get<string>(currentCodePiece.getValue())) += evaluate(rValue, 0, true);
					break;
				}

				case ECodeType::SUBTRACT_IS:
				{
					const CodePieceDeque rValue = getLine();
					*variableDictionary.at(get<string>(currentCodePiece.getValue())) -= evaluate(rValue, 0, true);
					break;
				}

				case ECodeType::MULTIPLY_IS:
				{
					const CodePieceDeque rValue = getLine();
					*variableDictionary.at(get<string>(currentCodePiece.getValue())) *= evaluate(rValue, 0, true);
					break;
				}

				case ECodeType::DIVIDE_IS:
				{
					const CodePieceDeque rValue = getLine();
					*variableDictionary.at(get<string>(currentCodePiece.getValue())) /= evaluate(rValue, 0, true);
					break;
				}

				case ECodeType::REMAINDER_IS:
				{
					const CodePieceDeque rValue = getLine();
					*variableDictionary.at(get<string>(currentCodePiece.getValue())) %= evaluate(rValue, 0, true);
					break;
				}

				default:
					ignoreLine();
					break;
				}
				break;
			}

			case ECodeType::FUNCTION:
			{
				const Value argument = getCodePiece().getValue();
				functionDictionary.at(get<string>(currentCodePiece.getValue()))(get<string>(argument));
				break;
			}

			case ECodeType::END_LINE:
				break;

			default:
			{
				ignoreLine();
			}
			}

			currentCodePiece = getCodePiece();
		}

		return true;
	}
	catch (...)
	{
		#ifdef _CodeRunAssertion
			throw;
		#else
			return false;
		#endif
	}
}
