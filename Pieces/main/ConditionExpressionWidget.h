/*
	ConditionExpressionWidget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "listWidget.h"
#include "SingleCodePieces.h"

class ConditionExpressionWidget : public listWidget
{
public:
	ConditionExpressionWidget(Position pos, float w, float h, UIObject* parent);
	virtual void onDragObjectDropped(DragObject* obj)override;
	virtual void alignObjects()override;
	virtual void addObjectToList(UIObject* obj)override;
	virtual void removeObjectFromList(UIObject* obj);
	virtual void update() override;
	virtual void draw()override;
	virtual void onClicked()override;
	virtual void onReleased()override;
	vector<string> getCodePiecesToWords();
	vector<CodePiece> getInterpreterCodePieces();
private:
	list<SingleCodePieces*> codePieces;
};

