/*
	SingleCodePieces.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "DraggableCodePieceObject.h"

class SingleCodePieces : public DraggableCodePieceObject
{
public:
	SingleCodePieces(Position pos, float w, float h, UIObject* parent, int type,const CodePiece& codeType, const string& nWord);
	virtual void draw() override;
	virtual void alignObjects()override;
	virtual vector<string> getCodePiecesToWords() override;
	virtual vector<CodePiece> getInterpreterCodePieces()override;
private:
};

