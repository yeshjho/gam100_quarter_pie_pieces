/*
	UI.h

	GAM100, Fall 2019

	JoonHo Hwang edited the draw part
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "list"
#include <unordered_map>
#include <string>
#include "common.h"
#include "UIObject.h"
#include "FragmentsWidget.h"
#include "wordMakingAreaWidget.h"
#include "HoldingWordsWidget.h"
#include "CodingAreaWidget.h"
#include "DragObj.h"

using namespace std;

class UI : public UIObject
{
	friend class FragmentsWidget;
	friend class wordMakingAreaWidget;
public:
	UI(const Position pos,float w,float h);
	virtual ~UI() override;
	virtual void draw();
	virtual void update();

	virtual void onClicked();
	virtual void onReleased();
	void onMouseClicked();
	void onMouseReleased();

	void addFragment(char fragLetter,int n);
	void addToBuffer(char fragLetter);
	void addToHoldingWords(const string& word);

	void registerUIButtonObject(ButtonObject* obj);
	void registerUIDragObject(DragObject* obj);
	void removeUIButtonObject(ButtonObject* obj);
	void removeUIDragObject(DragObject* obj);

	void reset();
private:
	void alignObjects();

	list<ButtonObject*> UIButtonObjects;
	list<DragObject*> UIDragObject;
    unordered_map<char, FragmentButton*> fragmentsTable;
	FragmentsWidget fragmentsWidget;
	wordMakingAreaWidget wordmakingareaWidget;
	HoldingWordsWidget holdingwordsWidget;
	CodingAreaWidget codingAreaWidget;
	DragObject* currentDraggingObject = nullptr;
};
