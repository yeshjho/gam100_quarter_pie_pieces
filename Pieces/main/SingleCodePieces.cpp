#include "SingleCodePieces.h"
#include "doodle/doodle.hpp"
#include "constants.h"
using namespace doodle;


SingleCodePieces::SingleCodePieces(Position pos, float w, float h, UIObject* parent, int type, const CodePiece& codeType, const string& nWord)
	:DraggableCodePieceObject(pos,w,h,parent,type,codeType,nWord)
{
}


void SingleCodePieces::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text(word, position.x, position.y + height / 2.f);
	pop_settings();
}

void SingleCodePieces::alignObjects()
{

}

vector<string> SingleCodePieces::getCodePiecesToWords()
{
	return { word };
}

vector<CodePiece> SingleCodePieces::getInterpreterCodePieces()
{
	return {codeType};
}

