#include "Bullet.h"
#include "global.h"
#include <doodle/doodle.hpp>

using namespace doodle;

Bullet::Bullet(const Vector& pos, bool isGoingLeft) : Object(BULLET, pos, { velocityX * (isGoingLeft ? -1 : 1), 0 }) {}

Bullet::~Bullet() {}

void Bullet::update()
{
	if (isDestroyed)
	{
		return;
	}

	move();
	checkCollisionWithPlayer();
}

void Bullet::draw()
{
	if (isDestroyed)
	{
		return;
	}

	push_settings();
	set_rectangle_mode(RectMode::Center);
	set_fill_color(255);
	draw_rectangle(position.getX(), position.getY(), width, height);
	pop_settings();
}

void Bullet::move()
{
	position += velocity;
	if (position.getX() < 0 - width || position.getX() > Width)
	{
		onDead();
	}
}

void Bullet::checkCollisionWithPlayer()
{
	const Vector player_position = player->getPos();
	const float x = position.getX();
	const float y = position.getY();

	if (powf(x - player_position.getX(), 2.f) + powf(y - player_position.getY(), 2.f) <= powf(width, 2.f))
	{
		player->takeDamage(damage);
		onDead();
	}
}

void Bullet::onDead()
{
	isDestroyed = true;
}
