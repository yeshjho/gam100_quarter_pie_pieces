/*
	common.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

class Vector
{
public:
	Vector(float x, float y);
	Vector(const Vector& other);
	
	void setXY(float nX, float nY);
	void setX(float nX);
	void setY(float nY);
	float getX() const;
	float getY() const;
	float getMagnitudeSqr() const;
	float getMagnitude() const;
	void scaleBy(float s);
	Vector scale(float s) const;
	
	Vector operator+(const Vector& other);
	Vector operator-(const Vector& other);
	Vector& operator +=(const Vector& other);
	Vector& operator -=(const Vector& other);
	Vector& operator =(const Vector& other);
	
	float dotProduct(const Vector& other) const;
	
	
private:
	float x;
	float y;
};


struct Speed
{
	float speedX;
	float speedY;
};

struct Position
{
	float x;
	float y;
};

enum class STATE { SPLASH_SCREEN, MAIN_MENU, IN_GAME, CODING };