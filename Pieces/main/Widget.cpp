#include "Widget.h"

Widget::Widget(const Position pos, float w, float h, UIObject* parent)
	:UIObject(pos,w,h),pParent(parent)
{
}

Widget::~Widget()
{
}

bool Widget::isDragObjectInTheBorder(UIObject* obj)
{
	float objWidth = obj->getWidth();
	float objHeight = obj->getHeight();

	float objLeftX = obj->getPosX();
	float objBottomY = obj->getPosY();
	float objRightX = objLeftX + objWidth;
	float objTopY = objBottomY + objHeight;

	float widgetLeftX = position.x;
	float widgetRightX = position.x + width;
	float widgetTopY = position.y + height;
	float widgetBottomY = position.y;

	bool is_obj_x_InTheBorder = (objLeftX<widgetRightX && objLeftX>widgetLeftX) || (objRightX<widgetRightX && objRightX>widgetLeftX);
	bool is_obj_y_InTheBorder = (objTopY<widgetTopY && objTopY>widgetBottomY) || (objBottomY > widgetBottomY && objBottomY < widgetTopY);
	
	return is_obj_x_InTheBorder && is_obj_y_InTheBorder;
}

UIObject* Widget::getParent()
{
	return pParent;
}
