﻿#include "game.h"
#include "map.h"
#include "global.h"

using namespace doodle;

void keyInput(KeyboardButtons);
void keyReleased(KeyboardButtons);
void mousePressed(MouseButtons);
void mouseReleased(MouseButtons);

void setup()
{
	set_frame_of_reference(RightHanded_OriginBottomLeft);
	set_rectangle_mode(RectMode::Corner);

	gameState = STATE::IN_GAME;

	game.loadMap(4);
}

void keyInput(KeyboardButtons)
{
	switch(gameState)
	{
		case STATE::IN_GAME :
			draw_text(to_string(Key), Width / 2.f, Height / 2.f);
			switch (Key)
			{
				case KeyboardButtons::A:
					player->setIsLeft(true);
					player->setIsRight(false);
		    		player->setSeeLeft(true);
					break;
				case KeyboardButtons::D:
		    		player->setIsLeft(false);
		    		player->setIsRight(true);
		    		player->setSeeLeft(false);
		    		break;
				case KeyboardButtons::Space:
		    		if (player->getCanJump())
					{
						player->jump();
		    		}
					break;
	    		case KeyboardButtons::J:
		    		if (player->getCanSlash())
					{
			    		player->setIsSlash(true);
					}
					break;
				case KeyboardButtons::P:
					gameState = STATE::CODING;
					break;
				case KeyboardButtons::W:
					for (Object* obj : game.getObjects())
					{
						if (obj->getType() == PORTAL)
						{
							((Portal*)obj)->activate();
						}
						else if(obj->getType() == CHEST)
						{
							((Chest*)obj)->open();
						}
					}
					break;
				default:
		    		break;
			}
			break;
		case STATE::CODING :
			if (Key == KeyboardButtons::Escape)
				gameState = STATE::IN_GAME;
			break;
		case STATE::MAIN_MENU :
			switch (Key)
			{
			case KeyboardButtons::S:
				setup();
				break;
			default:
				break;
			}
		default:
			break;
	}
}

void keyReleased(KeyboardButtons button)
{
	switch(gameState)
	{
	case STATE::MAIN_MENU:
		break;
	case STATE::IN_GAME :
	if (button == KeyboardButtons::A)
	{
		player->setIsLeft(false);
	}
	if (button == KeyboardButtons::D)
	{
		player->setIsRight(false);
	}
		break;
	case STATE::CODING :
		break;
	}
}

void mousePressed(MouseButtons buttons)
{
	if(buttons == MouseButtons::Left)
	{
		switch (gameState)
		{
		case STATE::CODING:
		   ui.onMouseClicked();
		   break;
		}
	}
}

void mouseReleased(MouseButtons buttons)
{
	if (buttons == MouseButtons::Left)
	{
		switch (gameState)
		{
		case STATE::CODING:
			ui.onMouseReleased();
			break;
		}
	}
}

//void on_mouse_released(MouseButtons button)
//{
//
//}


int main(void)
{
	create_window();
	toggle_full_screen();

	set_callback_key_pressed(&keyInput);
	set_callback_key_released(&keyReleased);
	set_callback_mouse_pressed(&mousePressed);
	set_callback_mouse_released(&mouseReleased);

#if !defined(_DEBUG) && !defined(_CodeRunAssertion)
	// DigiPen logo splash
	unsigned short framesToDisplayLogo = LOGO_DISPLAY_FRAME;
	Texture digipenLogo;
	digipenLogo.LoadFromPNG("digipenLogo.png");
	set_texture_mode(RectMode::Center);
	
	while (!is_window_closed() && framesToDisplayLogo-- > 0)
	{
		update_window();
		clear_background(255);
		draw_texture(digipenLogo, 0, 0);
		push_settings();
		set_fill_color(0);
		set_font_size(23);
		draw_text("All content (C) 2019 DigiPen(USA) Corporation, all rights reserved.", -500.f, -300.f);
		pop_settings();
	}

	gameState = STATE::MAIN_MENU;
	// DigiPen logo splash end
#endif

	while (!is_window_closed())
	{
		update_window();
		clear_background();
		game.update();
		game.draw();
		ui.update();
		ui.draw();
	}

	return 0;
}
