#include "CodeBodyWidget.h"
#include "constants.h"
#include "doodle/doodle.hpp"
using namespace doodle;

CodeBodyWidget::CodeBodyWidget(Position pos, float w, float h, UIObject* parent)
	:listWidget(pos,w,h,parent)
{}

CodeBodyWidget::~CodeBodyWidget()
{}

void CodeBodyWidget::onClicked()
{
}

void CodeBodyWidget::onReleased()
{
}

void CodeBodyWidget::update()
{
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		codePiece->update();
	}
}

void CodeBodyWidget::draw()
{
	push_settings();
	set_fill_color(0, 0, 255, 255);
	draw_rectangle(position.x, position.y, width, height);
	pop_settings();
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		codePiece->draw();
	}
}


void CodeBodyWidget::onDragObjectDropped(DragObject* obj)
{
	int type = obj->getType();
	if (type == DRAGGABLE_WORD_OBJECT)
		return;

	obj->setIsClicked(false);
	listWidget* parentWidget = static_cast<listWidget*>(obj->getParent());
	parentWidget->removeObjectFromList(obj);

	addObjectToList(obj);
	alignObjects();
}

void CodeBodyWidget::addObjectToList(UIObject* obj)
{
	listWidget::addObjectToList(obj);
	codePieces.push_back(static_cast<DraggableCodePieceObject*>(obj));
	autoResize();
}

void CodeBodyWidget::removeObjectFromList(UIObject* obj)
{
	listWidget::removeObjectFromList(obj);
	codePieces.remove(static_cast<DraggableCodePieceObject*>(obj));
	autoResize();
}

void CodeBodyWidget::alignObjects()
{
	float accumulatedHeight = 0.f;
	int columnCounter = 0;
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		if (codePiece->getType() == SINGLE_CODE_PIECE)
		{
			float objHeight = codePiece->getHeight();
			float newPosX = position.x + (float)columnCounter * codePiece->getWidth();
			float newPosY = position.y + height - objHeight - accumulatedHeight;
			codePiece->setPosXY(newPosX, newPosY);
			columnCounter = (columnCounter + 1) % MaxSingleCodePiecePerLine;
			if (columnCounter == 0)
				accumulatedHeight += objHeight;
		}
		else
		{
			if (columnCounter > 0)
			{
				columnCounter = 0;
				//singlePiece height.
				accumulatedHeight += singleCodePieceHeight;
			}
			float objHeight = codePiece->getHeight();
			float newPosY = position.y + height - objHeight - accumulatedHeight;
			codePiece->setPosXY(position.x, newPosY);
			codePiece->alignObjects();
			accumulatedHeight += objHeight;
			columnCounter = 0;
		}
	}
}

float CodeBodyWidget::getHeight() const
{
	return height;
}

void CodeBodyWidget::autoResize()
{
	float accumulatedHeight = 0.f;
	int columnCounter = 0;
	if (codePieces.size() > 0)
	{
		for (DraggableCodePieceObject* codePiece : codePieces)
		{
			if (codePiece->getType() == SINGLE_CODE_PIECE)
			{
				float objHeight = codePiece->getHeight();
				columnCounter = (columnCounter + 1) % MaxSingleCodePiecePerLine;
				if (columnCounter == 1)
					accumulatedHeight += objHeight;
			}
			else
			{
				if (columnCounter > 0)
				{
					columnCounter = 0;
					//singlePiece height.
					accumulatedHeight += singleCodePieceHeight;
				}
				float objHeight = codePiece->getHeight();
				accumulatedHeight += objHeight;
				columnCounter = 0;
			}
		}
		height = accumulatedHeight;
	}
	else
	{
		height = 45.f;
	}
}

vector<string> CodeBodyWidget::getCodePiecesToWords()
{
	vector<string> returnWords;
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		const vector<string>& words = codePiece->getCodePiecesToWords();
		returnWords.insert(returnWords.end(), words.begin(), words.end());
	}
	return returnWords;
}

vector<CodePiece> CodeBodyWidget::getInterpreterCodePieces()
{
	vector<CodePiece> CodePieces;
	int endLineCounter = 0;
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		int codeType = codePiece->getType();
		const vector<CodePiece>& tmp = codePiece->getInterpreterCodePieces();
		CodePieces.insert(CodePieces.end(), tmp.begin(), tmp.end());
		if(codeType == SINGLE_CODE_PIECE)
		{
			endLineCounter++;
			if (endLineCounter == 8)
			{
				CodePieces.push_back(CodePiece(ECodeType::END_LINE));
				endLineCounter = 0;
			}
		}
		else
		{
			if (endLineCounter > 0)
				CodePieces.push_back(CodePiece(ECodeType::END_LINE));
			endLineCounter = 0;
		}
	}

	return CodePieces;
}
