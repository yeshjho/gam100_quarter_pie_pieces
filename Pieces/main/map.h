/*
	map.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim
	Junhyuk Cha Wrote this all

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/doodle.hpp>
#include "common.h"

using namespace std;
using namespace doodle;
void draw_map();
extern vector <Position> blockPosition;
extern int map[8][25][45];