/*
	Portal.h

	GAM100, Fall 2019

	JoonHo Hwang implemented
	Doyoon Kim designed the structure
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

class Portal : public Object
{
public:
	Portal(const Vector& pos);
	~Portal() override;

	void update() override;
	void draw() override;
	void move() override {}

	void between_blocks() override {}
	void checkCollisionWithPlayer() override;
	void onDead() override {}

	void addForce(const Vector&) override {}

	void activate() const;

private:
	float width = 50;
	float height = 80;

	bool canBeActivated = false;
};

