/*
	ConditionalPieces.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "DraggableCodePieceObject.h"
#include "CodeBodyWidget.h"

class ConditionalPieces : public DraggableCodePieceObject
{
public:
	ConditionalPieces(Position pos, float w, float h, UIObject* parent, int type, const CodePiece& rCodeType, const string& nWord);
	virtual ~ConditionalPieces()override;
	virtual void RegisterWidgetsTo(listWidget* codingAreaWidget) = 0;
	virtual void DeRegisterWidgetsFrom(listWidget* codingAreaWidget) = 0;
	virtual void draw() override = 0;
	virtual float getHeight()const = 0;
	virtual void setPosXY(float n_x, float n_y)override;
protected:
	CodeBodyWidget codeBodyWidget;
};

