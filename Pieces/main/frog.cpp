#include "frog.h"
#include "map.h"
#include "player.h"
#include <doodle/doodle.hpp>
#include "global.h"
using namespace doodle;

Frog::Frog(const Vector& pos) : Object(FROG, pos)
{}

Frog::Frog(const Vector& pos, const Vector& spd) : Object(FROG, pos, spd)
{}

Frog::~Frog()
{}

void Frog::update()
{
	switch (state)
	{
		case FrogState::DEAD:
			break;
		case FrogState::ALIVE:
			velocity += acceleration;
			position += velocity;
			acceleration.setXY(0.f, 0.f);
			between_blocks();
			addForce(Vector(0.f, GRAVITY));
			move();
			checkCollisionWithPlayer();
			if (frog_jump == false && is_on_ground)
			{
				check_is_cooldown();
			}
			if (can_give_damage == false)
			{
				cooldown_damage();
			}
			break;
	}
}

void Frog::draw()
{
	switch (state)
	{
		case FrogState::DEAD:
			break;
		case FrogState::ALIVE:
			push_settings();
			set_rectangle_mode(RectMode::Center);
			set_fill_color(0, 200, 0);
			draw_rectangle(position.getX(), position.getY(), width, height);
			pop_settings();
			break;
	}
}

void Frog::move()
{
	if(frog_jump)
	{
		addForce(Vector(0.f, jump_value));
		addForce(Vector(moving_left_right, 0.f));
		check_current_cooldown = 0;
		is_on_ground = false;
		frog_jump = false;
	}
}

void Frog::addForce(const Vector& force)
{
	acceleration += force.scale(1.f / mass);
}

void Frog::between_blocks()
{
	for (int i = 0; i < blockPosition.size(); ++i)
	{
		//upper side
		if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y + BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
			is_on_ground = true;
		}
		//down side
		else if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2 && position.getY() < blockPosition[i].y - BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
		}
		//left side
		else if (position.getX() < blockPosition[i].x - BLOCK_WIDTH / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2);
			moving_left_right = -20.f;
		}
		//right side
		else if (position.getX() > blockPosition[i].x + BLOCK_WIDTH / 2 && position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2);
			moving_left_right = 20.f;
		}
	}
	//upper side
	if (position.getY() >= ((MAP_HEIGHT - 1) * BLOCK_HEIGHT) + 25)
	{
		position.setY((MAP_HEIGHT - 1) * BLOCK_HEIGHT + 25);
	}
	//down side
	else if (position.getY() <= (BLOCK_HEIGHT * (MAP_HEIGHT - 21)) + 25)
	{
		position.setY(BLOCK_HEIGHT * (MAP_HEIGHT - 21) + 25);
	}
	//left side
	else if (position.getX() <= (BLOCK_WIDTH * (MAP_WIDTH - 43)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 43) - 5);
	}
	//right side
	else if (position.getX() >= (BLOCK_WIDTH * (MAP_WIDTH - 1)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 1) - 5);
	}
}



void Frog::checkCollisionWithPlayer()

{
	Vector player_position = player->getPos();
	if (sqrt(powf(position.getX() - player_position.getX(), 2.f) + powf(position.getY() - player_position.getY(), 2.f)) <= 20 && can_give_damage)
	{
		player->takeDamage(damage);
		check_current_damage_cooldown = 0;
		can_give_damage = false;
	}
}

void Frog::onDead()
{
	//give words or letters
	int get_rand_value = random(0, 100);
	if (get_rand_value <= 5)
	{
		ui.addToHoldingWords("frog");
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('p', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('l', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('a', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('y', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('e', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('f', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('o', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 10)
	{
		ui.addFragment('w', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 10)
	{
		ui.addFragment('!', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 10)
	{
		ui.addFragment('|', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 10)
	{
		ui.addFragment('&', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('r', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('u', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('>', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('m', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('p', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('i', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('g', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('t', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('6', 1);
	}
	if (get_rand_value <= 1)
	{
		ui.addFragment('z', 1);
	}
	if (get_rand_value <= 5)
	{
		ui.addFragment('h', 1);
	}
	set_current_state(FrogState::DEAD);
}

void Frog::check_is_cooldown()
{
	check_current_cooldown += DeltaTime;
	if (check_current_cooldown > 0.5f)
	{
		frog_jump = true;
	}
}

void Frog::cooldown_damage()
{
	check_current_damage_cooldown += DeltaTime;
	if (check_current_damage_cooldown > 1.0f)
	{
		can_give_damage = true;
	}
}

void Frog::set_current_state(FrogState State)
{
	state = State;
}

FrogState Frog::get_current_state()
{
	return state;
}

void Frog::set_health(int Health)
{
	health = Health;
}

int Frog::get_health()
{
	return health;
}

void Frog::setJumpValue(float jumpValue)
{
	jump_value = jumpValue;
}

void Frog::set_damage(float Damage)
{
	damage = Damage;
}
