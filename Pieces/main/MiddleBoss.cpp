#include "MiddleBoss.h"
#include "map.h"
#include "player.h"
#include <doodle/doodle.hpp>
#include "global.h"
using namespace doodle;

MiddleBoss::MiddleBoss(const Vector& pos) : Object(MIDDLE_BOSS, pos)
{}

MiddleBoss::MiddleBoss(const Vector & pos, const Vector & spd) : Object(MIDDLE_BOSS, pos, spd)
{}

MiddleBoss::~MiddleBoss()
{}

void MiddleBoss::update()
{
	switch (state)
	{
		case MiddleBossState::DEAD:
			break;
		case MiddleBossState::ALIVE:
			velocity += acceleration;
			position += velocity;
			acceleration.setXY(0.f, 0.f);
			between_blocks();
			addForce(Vector(0.f, GRAVITY));
			move();
			checkCollisionWithPlayer();
			if (middle_boss_jump == false && is_on_ground)
			{
				check_is_cooldown();
			}
			if (can_give_damage == false)
			{
				cooldown_damage();
			}
			break;
	}
}

void MiddleBoss::draw()
{
	switch (state)
	{
		case MiddleBossState::DEAD:
			break;
		case MiddleBossState::ALIVE:
			push_settings();
			set_rectangle_mode(RectMode::Center);
			set_fill_color(34, 255, 0);
			draw_rectangle(position.getX(), position.getY(), width, height);
			pop_settings();
			break;
	}
}

void MiddleBoss::move()
{
	if (middle_boss_jump)
	{
		addForce(Vector(0.f, jump_value));
		addForce(Vector(moving_left_right, 0.f));
		check_current_cooldown = 0;
		is_on_ground = false;
		middle_boss_jump = false;
	}
}

void MiddleBoss::addForce(const Vector& force)
{
	acceleration += force.scale(1.f / mass);
}

void MiddleBoss::between_blocks()
{
	for (int i = 0; i < blockPosition.size(); ++i)
	{
		//upper side
		if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y + BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
			is_on_ground = true;
		}
		//down side
		else if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2 && position.getY() < blockPosition[i].y - BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
		}
		//left side
		else if (position.getX() < blockPosition[i].x - BLOCK_WIDTH / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2);
			moving_left_right = -60.f;
		}
		//right side
		else if (position.getX() > blockPosition[i].x + BLOCK_WIDTH / 2 && position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2);
			moving_left_right = 60.f;
		}
	}
	//upper side
	if (position.getY() >= ((MAP_HEIGHT - 1) * BLOCK_HEIGHT) + 25)
	{
		position.setY((MAP_HEIGHT - 1) * BLOCK_HEIGHT + 25);
	}
	//down side
	else if (position.getY() <= (BLOCK_HEIGHT * (MAP_HEIGHT - 21)) + 25)
	{
		position.setY(BLOCK_HEIGHT * (MAP_HEIGHT - 21) + 25);
	}
	//left side
	else if (position.getX() <= (BLOCK_WIDTH * (MAP_WIDTH - 43)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 43) - 5);
	}
	//right side
	else if (position.getX() >= (BLOCK_WIDTH * (MAP_WIDTH - 1)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 1) - 5);
	}
}



void MiddleBoss::checkCollisionWithPlayer()

{
	Vector player_position = player->getPos();
	if (sqrt(powf(position.getX() - player_position.getX(), 2.f) + powf(position.getY() - player_position.getY(), 2.f)) <= width / 2 + 10 && can_give_damage)
	{
		player->takeDamage(damage);
		check_current_damage_cooldown = 0;
		can_give_damage = false;
	}
}

void MiddleBoss::onDead()
{
	//give words or letters
	int get_rand_value = random(0, 100);
	if (get_rand_value <= 100)
	{
		ui.addToHoldingWords("health");
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 100)
	{
		ui.addFragment('x', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 5)
	{
		ui.addToHoldingWords("damage");
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('9', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('0', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('*', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('/', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('%', 1);
	}
	set_current_state(MiddleBossState::DEAD);
}

void MiddleBoss::check_is_cooldown()
{
	check_current_cooldown += DeltaTime;
	if (check_current_cooldown > 0.5f)
	{
		middle_boss_jump = true;
	}
}

void MiddleBoss::cooldown_damage()
{
	check_current_damage_cooldown += DeltaTime;
	if (check_current_damage_cooldown > 1.0f)
	{
		can_give_damage = true;
	}
}

void MiddleBoss::set_current_state(MiddleBossState State)
{
	state = State;
}

MiddleBossState MiddleBoss::get_current_state()
{
	return state;
}

void MiddleBoss::set_health(int Health)
{
	health = Health;
}

int MiddleBoss::get_health()
{
	return health;
}

float MiddleBoss::get_width()
{
	return width;
}

void MiddleBoss::setJumpValue(float jumpValue)
{
	jump_value = jumpValue;
}

void MiddleBoss::set_damage(float Damage)
{
	damage = Damage;
}
