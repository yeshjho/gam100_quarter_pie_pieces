#include "Object.h"

Object::Object(char type, const Vector& pos)
	:type(type),position(pos),velocity(0.f,0.f),acceleration(0.f,0.f)
{
}

Object::Object(char type, const Vector& pos, const Vector& vel)
	:type(type),position(pos),velocity(vel),acceleration(0.f,0.f)
{
}

Object::~Object()
{
}

char Object::getType()const
{
	return type;
}

Vector Object::getPos()const
{
	return position;
}

void Object::setPos(const Vector& pos)
{
	position = pos;
}

Vector Object::getVelocity()const
{
	return velocity;
}

void Object::setVelocity(const Vector& vel)
{
	velocity = vel;
}
