#include "combineButton.h"
#include "wordMakingAreaWidget.h"
#include "doodle/doodle.hpp"
#include "constants.h"
using namespace doodle;

CombineButton::CombineButton(const Position pos, float w, float h, UIObject* parent)
	:ButtonObject(pos,w,h,parent)
{
}

CombineButton::~CombineButton()
{
}

void CombineButton::onClicked()
{
	wordMakingAreaWidget* p_wordMakingAreaWidget = static_cast<wordMakingAreaWidget*>(pParent);
	p_wordMakingAreaWidget->combine();
}

void CombineButton::onReleased()
{
}

void CombineButton::update()
{
}

void CombineButton::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text("Combine", position.x + 5.f, position.y);
	pop_settings();
}
