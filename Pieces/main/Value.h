/*
	Value.h

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Doyoon Kim
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

#include <variant>
#include <string>
#include <cmath>


namespace UserCodeInterpreter
{
	class Value : public std::variant<double, std::string>
	{
	public:
		Value() : std::variant<double, std::string>() {}

		Value(const double value, bool isHoldingValue = true) : std::variant<double, std::string>(value), isHoldingValue(isHoldingValue) {}

		Value(const std::string& value) : std::variant<double, std::string>(value), isHoldingValue(true) {}

		explicit inline operator bool() const
		{
			return bool(std::get<double>(*this));
		}

		Value& operator+=(const Value& rValue)
		{
			*this = std::get<double>(*this) + std::get<double>(rValue);
			return *this;
		}

		Value& operator-=(const Value& rValue)
		{
			*this = std::get<double>(*this) - std::get<double>(rValue);
			return *this;
		}

		Value& operator*=(const Value& rValue)
		{
			*this = std::get<double>(*this) * std::get<double>(rValue);
			return *this;
		}

		Value& operator/=(const Value& rValue)
		{
			*this = std::get<double>(*this) / std::get<double>(rValue);
			return *this;
		}

		Value& operator%=(const Value& rValue)
		{
			*this = std::fmod(std::get<double>(*this), std::get<double>(rValue));
			return *this;
		}

		bool getIsHoldingValue() const
		{
			return isHoldingValue;
		}
		
	private:
		bool isHoldingValue = false;
	};
}
