/*
	FinalBoss.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim designed the structure
	Junhyuk Cha implemented

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "object.h"

enum class FinalBossState
{
	DEAD, ALIVE
};

class FinalBoss : public Object
{
public:
	FinalBoss(const Vector& pos);
	FinalBoss(const Vector& pos, const Vector& spd);
	~FinalBoss() override;
	void update() override;
	void draw() override;
	void move() override;
	void addForce(const Vector& force) override;
	void between_blocks() override;
	void onDead() override;

	void check_is_cooldown();
	void checkCollisionWithPlayer() override;

	void cooldown_damage();
	void set_current_state(FinalBossState State);
	FinalBossState get_current_state();
	void set_health(int Health);
	int get_health();
	float get_width();
	void set_jump_value(float jumpValue);
	void set_damage(float Damage);

	void setJumpValue(float jumpValue);

private:
	float mass = 10.f;
	float width = 60.f;
	float height = 60.f;
	float jump_value = 70.f;
	bool final_boss_jump = false;
	bool is_on_ground = false;
	float moving_left_right = -60.f;
	float check_current_cooldown = 0;
	float check_current_damage_cooldown = 0;
	bool can_give_damage = true;
	FinalBossState state = FinalBossState::ALIVE;
	int health = 20;
	float damage = 3;
};