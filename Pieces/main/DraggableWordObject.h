/*
	DraggableWordObject.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "DragObj.h"
#include "constants.h"
using namespace std;

class DraggableWordObject : public DragObject
{
public:
	DraggableWordObject(const Position pos, float w, float h,UIObject* parent,const string& nWord,int nType);
	virtual ~DraggableWordObject()override;
	virtual void onClicked() override;
	virtual void onReleased() override;
	virtual void update() override;
	virtual void draw() override;
	string getWord();
private:
	string word;
};

