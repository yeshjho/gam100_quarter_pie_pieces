#include "Armor.h"
#include "global.h"

void Armor::give_armor()
{
	int get_rand_value = random(0, 100) + 1;
	Armor new_armor = Armor(1);


	if (get_rand_value > 20 && get_rand_value <= 90)
	{
		new_armor = Armor(2);
	}
	else if (get_rand_value <= 99)
	{
		new_armor = Armor(3);
	}
	else
	{
		new_armor = Armor(4);
	}
	player->getArmor(new_armor);
}
void Armor::give_xarmor()
{
	player->getArmor(Armor(3));
}
Armor::Armor(unsigned char tier)
{
	if (tier < 1 || tier > MAX_TIER)
	{
		throw "tier out of range";
	}

	this->tier = tier;
	this->damageReductionPercentage = damageReductionPerTier[tier - 1];
}

float Armor::getDamageReductionPercentage() const
{
	return damageReductionPercentage;
}

unsigned char Armor::getTier() const
{
	return tier;
}
