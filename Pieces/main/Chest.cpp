#include "Chest.h"
#include "global.h"
#include <doodle/doodle.hpp>
#include <iostream>

using namespace doodle;
Chest::Chest(const Vector& pos) : Object(CHEST, pos) {}

Chest::~Chest()
{}

void Chest::update()
{
	checkCollisionWithPlayer();
}

void Chest::draw()
{
	push_settings();
	set_rectangle_mode(RectMode::Center);
	no_outline();
	set_fill_color(HexColor (chest_fillcolor));
	draw_rectangle(position.getX(), position.getY(), width, height);
	pop_settings();
}

void Chest::checkCollisionWithPlayer()
{
	const Vector player_position = player->getPos();
	const float x = position.getX();
	const float y = position.getY();

	if (powf(x - player_position.getX(), 2.f) + powf(y - player_position.getY(), 2.f) <= powf(width, 2.f))
	{
		can_open = true;
	}
	else
	{
		can_open = false;
	}
}

void Chest::open()
{
	if (!can_open)
	{
		return;
	}
	//give words or letters
	int get_rand_value = random(0, 100);
	if (get_rand_value <= 5)
	{
		ui.addToHoldingWords("giveitem");
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addToHoldingWords("sword");
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 20)
	{
		ui.addToHoldingWords("armor");
	}
	get_rand_value = random(0, 100);
	/*if (get_rand_value <= 30)
	{
		ui.addFragment('+');
	}*/
	get_rand_value = random(0, 100);
	/*if (get_rand_value <= 30)
	{
		ui.addToHoldingWords('-');
	}*/
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addToHoldingWords("jump");
	}
	get_rand_value = random(0, 100);
	/*if (get_rand_value <= 30)
	{
		ui.addToHoldingWords('=');
	}*/

	can_open = false;
	chest_fillcolor = 0x4f0000ff;
}

