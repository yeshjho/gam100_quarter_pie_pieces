/*
	HoldingWordsWidget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "listWidget.h"
#include "UIObject.h"

using namespace std;

class HoldingWordsWidget :public listWidget
{
public:
	HoldingWordsWidget(const Position pos, float w, float h, UIObject* parent);
	virtual ~HoldingWordsWidget() override;
	virtual void onClicked()override;
	virtual void onReleased()override;
	virtual void update()override;
	virtual void draw()override;
	virtual void onDragObjectDropped(DragObject* obj)override;
	void addHoldingWord(const string& word);
	void reset();

private:
};

