#include "CodePieceFor.h"
#include "doodle/doodle.hpp"
#include "CodingAreaWidget.h"
#include "constants.h"
using namespace doodle;

constexpr float SpaceBetweenExpressionWidgets = 25.f;
constexpr float widgetWidthDivisor = 4.f;

CodePieceFor::CodePieceFor(Position pos, float w, float h, UIObject* parent, int type, const CodePiece& rCodeType, const string& nWord)
	:ConditionalPieces(pos, w, h, parent, type, rCodeType, nWord),
	assignmentPartWidget({ pos.x + w / 10.f , pos.y }, w / widgetWidthDivisor, h, parent),
	conditionPartWidget({ pos.x + w / 10.f + w / widgetWidthDivisor + SpaceBetweenExpressionWidgets,pos.y }, w / widgetWidthDivisor, h, parent),
	statementPartWidget({ pos.x + w / 10.f + w / widgetWidthDivisor*2.f + SpaceBetweenExpressionWidgets*2.f,pos.y }, w / widgetWidthDivisor, h, parent)
{
} 

CodePieceFor::~CodePieceFor()
{
}

void CodePieceFor::RegisterWidgetsTo(listWidget* codingAreaWidget)
{
	CodingAreaWidget* pcodingAreaWidget = static_cast<CodingAreaWidget*>(codingAreaWidget);
	pcodingAreaWidget->codeWidgets.push_back(&assignmentPartWidget);
	pcodingAreaWidget->codeWidgets.push_back(&conditionPartWidget);
	pcodingAreaWidget->codeWidgets.push_back(&statementPartWidget);
	pcodingAreaWidget->codeWidgets.push_back(&codeBodyWidget);
}

void CodePieceFor::DeRegisterWidgetsFrom(listWidget* codingAreaWidget)
{
	CodingAreaWidget* pcodingAreaWidget = static_cast<CodingAreaWidget*>(codingAreaWidget);
	pcodingAreaWidget->codeWidgets.remove(&assignmentPartWidget);
	pcodingAreaWidget->codeWidgets.remove(&conditionPartWidget);
	pcodingAreaWidget->codeWidgets.remove(&statementPartWidget);
	pcodingAreaWidget->codeWidgets.remove(&codeBodyWidget);
}

void CodePieceFor::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text(word, position.x, position.y + height / 2.f);
	draw_text("i", assignmentPartWidget.getPosX() - 10.f, position.y);
	draw_text("i", conditionPartWidget.getPosX() - 10.f, position.y);
	draw_text("i", statementPartWidget.getPosX() - 10.f, position.y);
	pop_settings();
}

float CodePieceFor::getHeight() const
{
	return height+codeBodyWidget.getHeight();
}

void CodePieceFor::alignObjects()
{
	assignmentPartWidget.setPosXY(position.x + width / 10.f, position.y);
	conditionPartWidget.setPosXY(position.x + width / 10.f + width / widgetWidthDivisor + SpaceBetweenExpressionWidgets, position.y);
	statementPartWidget.setPosXY(position.x + width / 10.f + width / widgetWidthDivisor*2.f + SpaceBetweenExpressionWidgets*2.f, position.y);
	codeBodyWidget.setPosXY(position.x, position.y - codeBodyWidget.getHeight());
	assignmentPartWidget.alignObjects();
	conditionPartWidget.alignObjects();
	statementPartWidget.alignObjects();
	codeBodyWidget.alignObjects();
}

void CodePieceFor::resize(float w, float h)
{
	float ratio_dWidth = w / width;
	float ratio_dHeight = h / height;
	width = w;
	height = h;

	float exW = codeBodyWidget.getWidth();
	float exH = codeBodyWidget.getHeight();
	codeBodyWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	exW = assignmentPartWidget.getWidth();
	exH = assignmentPartWidget.getHeight();
	assignmentPartWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	exW = conditionPartWidget.getWidth();
	exH = conditionPartWidget.getHeight();
	conditionPartWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	exW = statementPartWidget.getWidth();
	exH = statementPartWidget.getHeight();
	statementPartWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	exW = codeBodyWidget.getWidth();
	exH = codeBodyWidget.getHeight();
	codeBodyWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	alignObjects();
}

void CodePieceFor::update()
{
	DragObject::update();
	alignObjects();
}

vector<string> CodePieceFor::getCodePiecesToWords()
{
	const vector<string>& wordsInassignmentPartWidget = assignmentPartWidget.getCodePiecesToWords();
	const vector<string>& wordsInconditionPartWidget = conditionPartWidget.getCodePiecesToWords();
	const vector<string>& wordsInstatementPartWidget = statementPartWidget.getCodePiecesToWords();
	const vector<string>& wordsInCodeBodyWidget = codeBodyWidget.getCodePiecesToWords();
	vector<string> words = wordsInassignmentPartWidget;

	for (const string& nWord : wordsInconditionPartWidget)
	{
		words.push_back(nWord);
	}
	for (const string& nWord : wordsInstatementPartWidget)
	{
		words.push_back(nWord);
	}
	for (const string& nWord : wordsInCodeBodyWidget)
	{
		words.push_back(nWord);
	}

	return words;
}

vector<CodePiece> CodePieceFor::getInterpreterCodePieces()
{
	vector<CodePiece> returnCodePieces;
	returnCodePieces.push_back(CodePiece(ECodeType::START));
	returnCodePieces.push_back(codeType);
	
	returnCodePieces.push_back(CodePiece(ECodeType::VARIABLE,string("i")));
	const vector<CodePiece>& assignmentWidgetCodePieces = assignmentPartWidget.getInterpreterCodePieces();
	returnCodePieces.insert(returnCodePieces.end(), assignmentWidgetCodePieces.begin(), assignmentWidgetCodePieces.end());
	returnCodePieces.push_back(CodePiece(ECodeType::END_LINE));
	returnCodePieces.push_back(CodePiece(ECodeType::BOUNDARY));
	
	returnCodePieces.push_back(CodePiece(ECodeType::VARIABLE, string("i")));
	const vector<CodePiece>& conditionWidgetCodePieces = conditionPartWidget.getInterpreterCodePieces();
	returnCodePieces.insert(returnCodePieces.end(), conditionWidgetCodePieces.begin(), conditionWidgetCodePieces.end());
	returnCodePieces.push_back(CodePiece(ECodeType::BOUNDARY));
	
	returnCodePieces.push_back(CodePiece(ECodeType::VARIABLE, string("i")));
	const vector<CodePiece>& statementWidgetCodePieces = statementPartWidget.getInterpreterCodePieces();
	returnCodePieces.insert(returnCodePieces.end(), statementWidgetCodePieces.begin(), statementWidgetCodePieces.end());
	returnCodePieces.push_back(CodePiece(ECodeType::END_LINE));

	returnCodePieces.push_back(CodePiece(ECodeType::BOUNDARY));
	const vector<CodePiece>& codeBodyWidgetCodePieces = codeBodyWidget.getInterpreterCodePieces();
	returnCodePieces.insert(returnCodePieces.end(), codeBodyWidgetCodePieces.begin(), codeBodyWidgetCodePieces.end());
	returnCodePieces.push_back(CodePiece(ECodeType::END));
	
	return returnCodePieces;

}

