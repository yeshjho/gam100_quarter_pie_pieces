/*
	crab.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

#include "common.h"
#include "constants.h"

class Object
{
public:
	Object(char type, const Vector& pos);
	Object(char type, const Vector& pos, const Vector& vel);
	virtual ~Object() = 0;

	virtual void update() = 0;
	virtual void draw() = 0;
	virtual void move() = 0;

	virtual void between_blocks() = 0;
	virtual void checkCollisionWithPlayer() = 0;

	virtual void onDead() = 0;

	char getType()const;

	Vector getPos()const;
	void setPos(const Vector& pos);

	Vector getVelocity()const;
	void setVelocity(const Vector& vel);

	virtual void addForce(const Vector& force) = 0;

protected:
	Vector position;
	Vector velocity;
	Vector acceleration;
	
	char type = 0;
};