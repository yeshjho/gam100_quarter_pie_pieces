/*
	player.h

	GAM100, Fall 2019

	JoonHo Hwang edited a little
	Doyoon Kim designed the structure
	Junhyuk Cha implemented

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
#include "crab.h"
#include "frog.h"
#include "MiddleBoss.h"
#include "FinalBoss.h"
#include "Sword.h"
#include "Armor.h"

class Player : public Object
{
public:
	Player(const Vector& pos);
	Player(const Vector& pos, const Vector& spd);

	~Player() override;

	/*enum class PLAYER_STATE{MovingRight,MovingLeft,Jumping,Attacking,Idle};*/

	void update()override;
	void draw()override;
	void move() override;
	void jump();
	void attack();
	void between_blocks() override;
	void addForce(const Vector& force) override;
	void onDead() override;
	float getHealth() const;
	void setHealth(float health);
	//void setState(PLAYER_STATE nState);
	void setCanJump(bool canJump);
	bool getCanJump();
	void setIsLeft(bool isLeft);
	bool getIsLeft();
	void setIsRight(bool isRight);
	bool getIsRight();
	void setIsJump(bool isJump);
	bool getIsJump();
	void setSeeLeft(bool seeLeft);
	bool getSeeLeft();
	void setIsSlash(bool isSlash);
	bool getIsSalsh();
	void setCanSlash(bool canSlash);
	bool getCanSlash();
	void takeDamage(float amount);
	void check_is_cooldown();
	void checkCollisionWithPlayer() override {};
	int get_sword_damage();
	void getSword(Sword sword);
	void getArmor(Armor armor);
	void getArmor(unsigned char tier);
	void getArmorColor();
	void get_gameCleared(bool gameCleared);

	float getJumpValue() const;
	void setJumpValue(float jumpValue);
	float getSpeed() const;
	void setSpeed(float speed);

public:
	static constexpr float MAX_HEALTH = 10;
private:
	float mass = 10.f;
	float width = 20.f;
	float height = 20.f;
	float jump_value = 75.f;
	float speed = 3;
	bool isMoving = false;
	bool can_jump = true;
	bool is_left = false;
	bool is_right = false;
	bool is_jump = false;
	bool see_left = false;
	bool is_slash = false;
	bool can_slash = true;
	float check_current_cooldown = 0;
	float health = MAX_HEALTH;
	int armor_color = 0xffffffff;
	bool game_cleared = false;
	Sword current_sword = {SwordType::NORMAL_SWORD};
	Armor currentArmor = Armor(1);
};
