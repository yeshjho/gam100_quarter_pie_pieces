/*
	CodingAreaWidget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "listWidget.h"
#include "DraggableCodePieceObject.h"
#include "CodePieceIF.h"
#include "CodePieceElse.h"
#include "CodePieceFor.h"
#include "CodePieceWhile.h"
#include "RunButton.h"

class CodingAreaWidget : public listWidget
{
	friend class CodePieceIf;
	friend class CodePieceElse;
	friend class CodePieceFor;
	friend class CodePieceWhile;
public:
	CodingAreaWidget(const Position pos, float w, float h, UIObject* parent);
	virtual ~CodingAreaWidget()override;
	virtual void onDragObjectDropped(DragObject* obj);
	virtual void addObjectToList(UIObject* obj);
	void addObjectToListFromHoldingWordsWidget(DraggableCodePieceObject* obj);
	void removeObjectFromListByHoldingWordsWidget(DraggableCodePieceObject* obj);
	virtual void removeObjectFromList(UIObject* obj);
	virtual void onClicked()override;
	virtual void onReleased()override;
	virtual void update()override;
	virtual void draw()override;
	virtual void alignObjects()override;
	void interPret();
	void reset();

private:
	list<DraggableCodePieceObject*> codePieces;
	list<listWidget*> codeWidgets;
	RunButton runbutton;
};
