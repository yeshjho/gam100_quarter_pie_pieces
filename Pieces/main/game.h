/*
	game.h

	GAM100, Fall 2019

	JoonHo Hwang edited a little & implemented the runInterpreter()
	Doyoon Kim designed the structure
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "player.h"
#include "crab.h"
#include "frog.h"
#include "UIObject.h"
#include "Portal.h"
#include "Bullet.h"
#include "Chest.h"
#include <vector>
#include "CodePiece.h"
using std::vector;

class Game
{
public:
	Game();
	~Game();
	void update();
	void draw() const;
	vector<Object*> getObjects();
	void loadMap(unsigned char stageNum, bool isPlayerPositionLeft=true);
	unsigned char getCurrentMapNumber() const;
	void addObject(Object* object);

	void runInterpreter(const vector<UserCodeInterpreter::CodePiece>& codePieces);

private:
	void clearObjects();

	vector<Object*> objects;

	unsigned char currentMapNumber = 255;


public:
	bool isPendingNewMap = false;
	unsigned char pendingMapNumber = 255;

private:
	vector<Object*> pendingObjects;
};
