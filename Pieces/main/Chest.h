/*
	Chest.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim
	Junhyuk Cha Wrote this all based on Portal.h

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

class Chest : public Object
{
public:
	Chest(const Vector& pos);
	~Chest() override;

	void update() override;
	void draw() override;
	void move() override {}

	void between_blocks() override {}
	void checkCollisionWithPlayer() override;
	void addForce(const Vector&) override {}

	void onDead() override {}

	void open();
private:
	float width = 30;
	float height = 30;
	int chest_fillcolor = 0xffff00ff;
	bool can_open = false;
};

