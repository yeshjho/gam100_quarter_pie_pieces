/*
	CodePiece.h
	
	GAM100, Fall 2019
	
	JoonHo Hwang Wrote this all
	Doyoon Kim
	Junhyuk Cha
	
	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

#include "Value.h"


namespace UserCodeInterpreter
{
	enum class ECodeType
	{
		IF,
		ELSE,

		WHILE,
		FOR,

		START,
		BOUNDARY,  // substitute for {s, :s, (;s in for loops)
		END,  // substitute for }s
		END_LINE,  // substitute for ;s at the end of an expression

		AND,
		OR,
		NOT,

		IS,
		ADD_IS,
		SUBTRACT_IS,
		MULTIPLY_IS,
		DIVIDE_IS,
		REMAINDER_IS,

		CONSTANT,
		VARIABLE,
		FUNCTION,

		ADD,
		SUBTRACT,
		MULTIPLY,
		DIVIDE,
		REMAINDER,

		NOT_EQUAL,
		EQUAL,
		GREATER,
		GREATER_EQUAL,
		LESSER,
		LESSER_EQUAL,

		PARENTHESIS_OPEN,
		PARENTHESIS_CLOSE,

		PROGRAM_END
	};

	class CodePiece
	{
	public:
		CodePiece(const ECodeType type);
		CodePiece(const ECodeType type, const Value& value) : type(type), value(value) {};

		Value getValue() const;
		ECodeType getType() const;
		unsigned char getLBP() const;

	private:
		Value value{};  // CONSTANT for its value, VARIABLE and FUNCTION for its name
		ECodeType type;
		unsigned char leftBindingPower = 0;
	};
}
