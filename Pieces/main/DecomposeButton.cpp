#include "DecomposeButton.h"
#include "wordMakingAreaWidget.h"
#include "doodle/doodle.hpp"
#include "constants.h"
using namespace doodle;

DecomposeButton::DecomposeButton(const Position pos, float w, float h, UIObject* parent)
	:ButtonObject(pos,w,h,parent)
{
}

DecomposeButton::~DecomposeButton()
{
}

void DecomposeButton::onClicked()
{
	wordMakingAreaWidget* p_wordMakingAreaWidget = static_cast<wordMakingAreaWidget*>(pParent);
	p_wordMakingAreaWidget->decompose();
}

void DecomposeButton::onReleased()
{
}

void DecomposeButton::update()
{
}

void DecomposeButton::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text("Decompose", position.x + 5.f, position.y);
	pop_settings();
}
