/*
	global.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim designed the structure
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "game.h"
#include "UI.h"
#include <doodle/doodle.hpp>
#include "common.h"

using namespace doodle;


inline Game game;
//the value of fullscreen
inline UI ui({ 1920.f / 2.f - 200.f,0 }, 1000.f,1080.f);
inline Player* player = new Player({0, 0});

//should be changed to MAIN_MENU later.
#if !defined(_DEBUG) && !defined(_CodeRunAssertion)
inline STATE gameState = STATE::SPLASH_SCREEN;
#else
inline STATE gameState = STATE::MAIN_MENU;
#endif

inline void give_item(const string& item)
{
	if (item == "armor")
	{
		Armor::give_armor();
	}
	else if (item == "sword")
	{
		Sword::give_sword();
	}
	else if (item == "xarmor")
	{
		Armor::give_xarmor();
	}
	else if (item == "xsword")
	{
		Sword::give_xsword();
	}
}