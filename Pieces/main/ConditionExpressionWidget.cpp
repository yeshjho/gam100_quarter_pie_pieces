#include "ConditionExpressionWidget.h"
#include "DraggableWordObject.h"
#include "HoldingWordsWidget.h"
#include "global.h"
#include "constants.h"
#include "doodle/doodle.hpp"

ConditionExpressionWidget::ConditionExpressionWidget(Position pos, float w, float h, UIObject* parent)
	:listWidget(pos,w,h,parent)
{
}

void ConditionExpressionWidget::onDragObjectDropped(DragObject* obj)
{
	int type = obj->getType();
	if (type == DRAGGABLE_WORD_OBJECT || type == CONDITIONAL_PIECE)
		return;

	//type == SINGLE_CODE_PIECE
	obj->setIsClicked(false);
	listWidget* parentWidget = static_cast<listWidget*>(obj->getParent());
	parentWidget->removeObjectFromList(obj);
	addObjectToList(obj);
	alignObjects();
}

void ConditionExpressionWidget::alignObjects()
{
	float accumulated_xPos = 0.f;
	for (SingleCodePieces* codePiece : codePieces)
	{
		float objWidth = codePiece->getWidth();
		float newPosX = position.x + accumulated_xPos;
		codePiece->setPosXY(newPosX, position.y);
		accumulated_xPos += objWidth;
	}
}

void ConditionExpressionWidget::addObjectToList(UIObject* obj)
{
	listWidget::addObjectToList(obj);
	codePieces.push_back(static_cast<SingleCodePieces*>(obj));
}

void ConditionExpressionWidget::removeObjectFromList(UIObject* obj)
{
	listWidget::removeObjectFromList(obj);
	codePieces.remove(static_cast<SingleCodePieces*>(obj));
}

void ConditionExpressionWidget::update()
{
	for (SingleCodePieces* codePiece : codePieces)
	{
		codePiece->update();
	}
}

void ConditionExpressionWidget::draw()
{
	push_settings();
	set_fill_color(255, 0, 0, 255);
	draw_rectangle(position.x, position.y, width, height);
	pop_settings();
	for (SingleCodePieces* codePiece : codePieces)
	{
		codePiece->draw();
	}
}

void ConditionExpressionWidget::onClicked()
{
}

void ConditionExpressionWidget::onReleased()
{
}

vector<string> ConditionExpressionWidget::getCodePiecesToWords()
{
	vector<string> returnWords;
	for (SingleCodePieces* codePiece : codePieces)
	{
		const vector<string>& words = codePiece->getCodePiecesToWords();
		returnWords.insert(returnWords.end(), words.begin(), words.end());
	}
	return returnWords;
}

vector<CodePiece> ConditionExpressionWidget::getInterpreterCodePieces()
{
	vector<CodePiece> CodePieces;
	for (SingleCodePieces* codePiece : codePieces)
	{
		const vector<CodePiece>& tmp = codePiece->getInterpreterCodePieces();
		CodePieces.insert(CodePieces.end(), tmp.begin(), tmp.end());
	}
	
	return CodePieces;
}
