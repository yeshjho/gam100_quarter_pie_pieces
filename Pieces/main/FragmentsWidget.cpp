#include "FragmentsWidget.h"
#include "UI.h"
#include "global.h"

FragmentsWidget::FragmentsWidget(const Position pos, float w, float h, UIObject* parent)
	:Widget(pos,w,h,parent)
{
	UIObject* thisAdress = static_cast<UIObject*>(this);
	UI* pUI = static_cast<UI*>(parent);

	float button_w = w / 6.f;
	float button_h = h / 8.f;

	//create buttons
	int i = 0;
	for(int row = 0; row< 8; row++)
	{
	    for(int col = 0; col < 6; col++)
	    {
			if (i == 48)
				break;
			Position buttonPos = { pos.x + (button_w * (float)(col)),pos.y + h - (button_h * (float)(row+1)) };
			char letter = fragments[i];
			
			FragmentButton* button = new FragmentButton(buttonPos, button_w, button_h, letter, initialFragmentQuantity, thisAdress);
			fragmentButtons.push_back(button);
			ui.registerUIButtonObject(button);
			pUI->fragmentsTable.insert({ letter,button });
			i++;
		}
	}
}

FragmentsWidget::~FragmentsWidget()
{
	for (FragmentButton* button : fragmentButtons)
	{
		if(button != nullptr)
			delete button;
	}
}

void FragmentsWidget::onClicked()
{
	
}

void FragmentsWidget::onReleased()
{
}

void FragmentsWidget::update()
{
	for (FragmentButton* button : fragmentButtons)
	{
		button->update();
	}
}

void FragmentsWidget::draw()
{
	push_settings();
	draw_rectangle(position.x, position.y, width, height);
	pop_settings();
	for (FragmentButton* button : fragmentButtons)
	{
		button->draw();
	}
}
