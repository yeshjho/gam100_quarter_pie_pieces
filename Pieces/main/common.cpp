#include "common.h"
#include <math.h>

Vector::Vector(float x, float y)
	:x(x),y(y)
{
	
}

Vector::Vector(const Vector& other)
	:x(other.x),y(other.y)
{
}

void Vector::setXY(float nX, float nY)
{
	x = nX;
	y = nY;
}

void Vector::setX(float nX)
{
	x = nX;
}

void Vector::setY(float nY)
{
	y = nY;
}

float Vector::getX() const
{
	return x;
}

float Vector::getY() const
{
	return y;
}

float Vector::getMagnitudeSqr() const
{
	return x * x + y * y;
}

float Vector::getMagnitude() const
{
	return sqrtf(x * x + y * y);
}

void Vector::scaleBy(float s)
{
	x *= s;
	y *= s;
}

Vector Vector::scale(float s)const
{
	Vector v(this->x, this->y);
	v.scaleBy(s);
	return v;
}

Vector Vector::operator+(const Vector& other)
{
	return Vector(x + other.x, y + other.y);
}

Vector Vector::operator-(const Vector& other)
{
	return Vector(x-other.x,y-other.y);
}

Vector& Vector::operator+=(const Vector& other)
{
	this->x += other.x;
	this->y += other.y;

	return *this;
}

Vector& Vector::operator-=(const Vector& other)
{
	this->x -= other.x;
	this->y -= other.y;

	return *this;
}

Vector& Vector::operator=(const Vector& other)
{
	this->x = other.x;
	this->y = other.y;

	return *this;
}

float Vector::dotProduct(const Vector& other)const
{
	return this->x * other.x + this->y * other.y;
}
