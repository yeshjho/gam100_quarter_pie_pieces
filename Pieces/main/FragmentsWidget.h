/*
	FragmentsWidget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "common.h"
#include "FragmentButton.h"
#include "Widget.h"
#include <vector>
using namespace std;

class FragmentsWidget :public Widget
{
public:
	FragmentsWidget(const Position pos,float w, float h,UIObject* parent);
	virtual ~FragmentsWidget() override;
	virtual void onClicked()override;
	virtual void onReleased()override;
	virtual void update()override;
	virtual void draw()override;

private:
	vector<char> fragments = { 'a','b','c','d','e','f',
							   'g','h','i','j','l','m',
							   'n','o','p','r','s','u',
							   't','v','w','x','y','z',
							   '1','2','3','4','5','6',
							   '7','8','9','0','!','&',
							   '|','+','-','%','/','*',
							   '=','<','>','(',')','.'
		                       };
	vector<FragmentButton*> fragmentButtons;
};

