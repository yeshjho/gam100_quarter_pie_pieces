#include "CodePieceIF.h"
#include "doodle/doodle.hpp"
#include "CodingAreaWidget.h"
using namespace doodle;

CodePieceIf::CodePieceIf(Position pos, float w, float h, UIObject* parent, int type, const CodePiece& rCodeType, const string& nWord)
	:ConditionalPieces(pos, w, h, parent, type, rCodeType, nWord),
	conditionExpressionWidget({pos.x+w/6.f,pos.y},w*5.f/6.f,h,parent)
{

}

CodePieceIf::~CodePieceIf()
{
}

void CodePieceIf::RegisterWidgetsTo(listWidget* codingAreaWidget)
{
	CodingAreaWidget* pcodingAreaWidget = static_cast<CodingAreaWidget*>(codingAreaWidget);
	pcodingAreaWidget->codeWidgets.push_back(&conditionExpressionWidget);
	pcodingAreaWidget->codeWidgets.push_back(&codeBodyWidget);
}

void CodePieceIf::DeRegisterWidgetsFrom(listWidget* codingAreaWidget)
{
	CodingAreaWidget* pcodingAreaWidget = static_cast<CodingAreaWidget*>(codingAreaWidget);
	pcodingAreaWidget->codeWidgets.remove(&conditionExpressionWidget);
	pcodingAreaWidget->codeWidgets.remove(&codeBodyWidget);
}

void CodePieceIf::draw()
{
	push_settings();
	set_font_size(15.f);
	draw_rectangle(position.x, position.y, width, height);
	draw_text(word, position.x, position.y);
	pop_settings();
}

float CodePieceIf::getHeight() const
{
	return height+codeBodyWidget.getHeight();
}

void CodePieceIf::alignObjects()
{
	conditionExpressionWidget.setPosXY(position.x + width / 6.f, position.y);
	codeBodyWidget.setPosXY(position.x, position.y - codeBodyWidget.getHeight());
	conditionExpressionWidget.alignObjects();
	codeBodyWidget.alignObjects();
}

void CodePieceIf::resize(float w, float h)
{
	float ratio_dWidth = w / width;
	float ratio_dHeight = h / height;
	width = w;
	height = h;

	float exW = conditionExpressionWidget.getWidth();
	float exH = conditionExpressionWidget.getHeight();
	conditionExpressionWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	exW = codeBodyWidget.getWidth();
	exH = codeBodyWidget.getHeight();
	codeBodyWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	alignObjects();
}

void CodePieceIf::update()
{
	DragObject::update();
	alignObjects();
}

vector<string> CodePieceIf::getCodePiecesToWords()
{
	const vector<string>& wordsInConditionExpressionWidget = conditionExpressionWidget.getCodePiecesToWords();
	const vector<string>& wordsInCodeBodyWidget = codeBodyWidget.getCodePiecesToWords();
	vector<string> words = wordsInConditionExpressionWidget;

	for (const string& nWord : wordsInCodeBodyWidget)
	{
		words.push_back(nWord);
	}

	return words;
}

vector<CodePiece> CodePieceIf::getInterpreterCodePieces()
{
	vector<CodePiece> returnCodePieces;
	returnCodePieces.push_back(CodePiece(ECodeType::START));
	returnCodePieces.push_back(codeType);
	
	const vector<CodePiece>& conditionWidgetCodePieces = conditionExpressionWidget.getInterpreterCodePieces();
	returnCodePieces.insert(returnCodePieces.end(), conditionWidgetCodePieces.begin(), conditionWidgetCodePieces.end());
	returnCodePieces.push_back(CodePiece(ECodeType::BOUNDARY));
	
	const vector<CodePiece>& codeBodyWidgetCodePieces = codeBodyWidget.getInterpreterCodePieces();
	returnCodePieces.insert(returnCodePieces.end(), codeBodyWidgetCodePieces.begin(), codeBodyWidgetCodePieces.end());
	returnCodePieces.push_back(CodePiece(ECodeType::END));
	
	return returnCodePieces;
}
