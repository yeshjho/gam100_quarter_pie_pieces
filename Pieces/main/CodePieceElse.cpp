#include "CodePieceElse.h"
#include "doodle/doodle.hpp"
#include "CodingAreaWidget.h"
#include "constants.h"
using namespace doodle;

CodePieceElse::CodePieceElse(Position pos, float w, float h, UIObject* parent, int type, const CodePiece& rCodeType, const string& nWord)
	:ConditionalPieces(pos, w, h, parent, type, rCodeType, nWord)
{
}

CodePieceElse::~CodePieceElse()
{
}

void CodePieceElse::RegisterWidgetsTo(listWidget* codingAreaWidget)
{
	CodingAreaWidget* pcodingAreaWidget = static_cast<CodingAreaWidget*>(codingAreaWidget);
	pcodingAreaWidget->codeWidgets.push_back(&codeBodyWidget);
}

void CodePieceElse::DeRegisterWidgetsFrom(listWidget* codingAreaWidget)
{
	CodingAreaWidget* pcodingAreaWidget = static_cast<CodingAreaWidget*>(codingAreaWidget);
	pcodingAreaWidget->codeWidgets.remove(&codeBodyWidget);
}

void CodePieceElse::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text(word, position.x, position.y + height / 2.f);
	pop_settings();
}

float CodePieceElse::getHeight() const
{
	return height+codeBodyWidget.getHeight();
}

void CodePieceElse::alignObjects()
{
	codeBodyWidget.setPosXY(position.x, position.y - codeBodyWidget.getHeight());
	codeBodyWidget.alignObjects();
}

void CodePieceElse::resize(float w, float h)
{
	float ratio_dWidth = w / width;
	float ratio_dHeight = h / height;
	width = w;
	height = h;

	float exW = codeBodyWidget.getWidth();
	float exH = codeBodyWidget.getHeight();
	codeBodyWidget.resize(exW * ratio_dWidth, exH * ratio_dHeight);

	alignObjects();
}

void CodePieceElse::update()
{
	DragObject::update();
	alignObjects();
}

vector<string> CodePieceElse::getCodePiecesToWords()
{
	const vector<string>& wordsInCodeBodyWidget = codeBodyWidget.getCodePiecesToWords();

	return wordsInCodeBodyWidget;
}

vector<CodePiece> CodePieceElse::getInterpreterCodePieces()
{
	vector<CodePiece> returnCodePieces;
	returnCodePieces.push_back(codeType);
	const vector<CodePiece>& codeBodyWidgetCodePieces = codeBodyWidget.getInterpreterCodePieces();
	returnCodePieces.insert(returnCodePieces.end(), codeBodyWidgetCodePieces.begin(), codeBodyWidgetCodePieces.end());
	returnCodePieces.push_back(CodePiece(ECodeType::END));
	
	return returnCodePieces;
}

