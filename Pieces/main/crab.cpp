#include "crab.h"
#include "map.h"
#include "player.h"
#include <doodle/doodle.hpp>
#include "global.h"
using namespace doodle;

Crab::Crab(const Vector& pos) : Object(CRAB,pos)
{
}

Crab::Crab(const Vector& pos, const Vector& spd) : Object(CRAB,pos,spd)
{
}

Crab::~Crab()
{
}

void Crab::update()
{
	switch (state)
	{
		case CrabState::DEAD:
			break;
		case CrabState::ALIVE:
		{
			addForce(Vector(0.f, GRAVITY));
			velocity += acceleration;
			position += velocity;
			move();
			acceleration.setXY(0.f, 0.f);
			between_blocks();
			checkCollisionWithPlayer();
			if (can_give_damage == false)
			{
				cooldown_damage();
			}
			break;
		}
	}
}

void Crab::draw()
{
	switch (state)
	{
		case CrabState::DEAD:
			break;
		case CrabState::ALIVE:
		{
			push_settings();
			set_rectangle_mode(RectMode::Center);
			set_fill_color(200, 0, 0);
			draw_rectangle(position.getX(), position.getY(), width, height);
			pop_settings();
			break;
		}
}
}

void Crab::move()
{
	position += Vector(moving_left_right, 0.f);
}

void Crab::addForce(const Vector& force)
{
	acceleration += force.scale(1.f / mass);
}

void Crab::between_blocks()
{
	for (int i = 0; i < blockPosition.size(); ++i)
	{
		//upper side
		if (position.getX() < blockPosition[i].x + BLOCK_WIDTH/2 + width/2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH/2 - width/2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT/2 + height/2 && position.getY() > blockPosition[i].y + BLOCK_HEIGHT/2)
		{
			position.setY(blockPosition[i].y + BLOCK_HEIGHT/2 + height/2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
		}
		//down side
		else if (position.getX() < blockPosition[i].x + BLOCK_WIDTH/2 + width/2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH/2 - width/2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT/2 - height/2 && position.getY() < blockPosition[i].y - BLOCK_HEIGHT/2)
		{
			position.setY(blockPosition[i].y - BLOCK_HEIGHT/2 - height/2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
		}
		//left side
		else if (position.getX() < blockPosition[i].x - BLOCK_WIDTH/2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH/2 - width/2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT/2 + height/2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT/2 - height/2)
		{
			position.setX(blockPosition[i].x - BLOCK_WIDTH/2 - width/2);
			moving_left_right = -2.0f;
		}
		//right side
		else if (position.getX() > blockPosition[i].x + BLOCK_WIDTH/2 && position.getX() < blockPosition[i].x + BLOCK_WIDTH/2 + width/2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT/2 + height/2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT/2 - height/2)
		{
			position.setX(blockPosition[i].x + BLOCK_WIDTH/2 + width/2);
			moving_left_right = 2.0f;
		}
	}
	//upper side
	if (position.getY() >= ((MAP_HEIGHT - 1) * BLOCK_HEIGHT) + 25)
	{
		position.setY((MAP_HEIGHT - 1) * BLOCK_HEIGHT + 25);
	}
	//down side
	else if (position.getY() <= (BLOCK_HEIGHT * (MAP_HEIGHT - 21)) + 25)
	{
		position.setY(BLOCK_HEIGHT * (MAP_HEIGHT - 21) + 25);
	}
	//left side
	else if (position.getX() <= (BLOCK_WIDTH * (MAP_WIDTH - 43)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 43) - 5);
	}
	//right side
	else if (position.getX() >= (BLOCK_WIDTH * (MAP_WIDTH - 1)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 1) - 5);
	}
}


void Crab::checkCollisionWithPlayer()

{
	Vector player_position = player->getPos();
	if(sqrt(powf(position.getX() - player_position.getX(), 2.f)+ powf(position.getY() - player_position.getY(), 2.f))<= 20 && can_give_damage)
	{
		player->takeDamage(damage);
		check_current_cooldown = 0;
		can_give_damage = false;
	}
}

void Crab::onDead()
{
	//give words or letters
	int get_rand_value = random(0, 100);
	if (get_rand_value <= 5)
	{
		ui.addToHoldingWords("crab");
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('r', 1);
	}
	if (get_rand_value <= 30)
	{
		ui.addFragment('v', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('a', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('p', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('l', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('y', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('e', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('r', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('2', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('j', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('s', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('d', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('i', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('t', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('n', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('(', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment(')', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('<', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 30)
	{
		ui.addFragment('5', 1);
	}
	get_rand_value = random(0, 100);
	if (get_rand_value <= 5)
	{
		ui.addFragment('*', 1);
	}
	set_current_state(CrabState::DEAD);
}

void Crab::set_damage(float Damage)
{
	damage = Damage;
}

void Crab::set_health(int Health)
{
	health = Health;
}

int Crab::get_health()
{
	return health;
}

void Crab::cooldown_damage()
{
	check_current_cooldown += DeltaTime;
	if (check_current_cooldown > 1.0f)
	{
		can_give_damage = true;
	}
}

void Crab::set_current_state(CrabState State)
{
	state = State;
}

CrabState Crab::get_current_state()
{
	return state;
}

