#include "DraggableCodePieceObject.h"



DraggableCodePieceObject::DraggableCodePieceObject(const Position pos, float w, float h, UIObject* parent, int type, const CodePiece& rCodeType, const string& nWord)
	:DragObject(pos,w,h,parent,type),codeType(rCodeType),word(nWord)
{
}

DraggableCodePieceObject::~DraggableCodePieceObject()
{
}

void DraggableCodePieceObject::onClicked()
{
	DragObject::onClicked();
}

void DraggableCodePieceObject::onReleased()
{
	DragObject::onReleased();
}

void DraggableCodePieceObject::resize(float w, float h)
{
	width = w;
	height = h;
}

string DraggableCodePieceObject::getWord() const
{
	return word;
}
