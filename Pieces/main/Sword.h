/*
	Sword.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim
	Junhyuk Cha Wrote this all

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <unordered_map>
enum class SwordType
{
	NORMAL_SWORD = 1, RARE_SWORD, EPIC_SWORD, LEGENDARY_SWORD
};
inline std::unordered_map<SwordType, int> swordDamage { {SwordType::NORMAL_SWORD, 1}, {SwordType::RARE_SWORD, 2},
{SwordType::EPIC_SWORD, 4}, {SwordType::LEGENDARY_SWORD, 6} };
inline std::unordered_map<SwordType, int> swordColor{ {SwordType::NORMAL_SWORD, 0xffffffff}, {SwordType::RARE_SWORD, 0x0000ffff},
{SwordType::EPIC_SWORD, 0xff00ffff}, {SwordType::LEGENDARY_SWORD, 0xffff00ff} };
class Sword
{
	public:
		Sword(SwordType sword_type);
		static void give_sword();
		static void give_xsword();
		int get_damage() const;
		int get_color() const;
	private:
		SwordType type;
		int damage = 0;
		int color = 0;
};