#include "DraggableWordObject.h"
#include "doodle/doodle.hpp"
using namespace doodle;

DraggableWordObject::DraggableWordObject(const Position pos, float w, float h, UIObject* parent, const string& nWord, int nType)
	:DragObject(pos,w,h,parent,nType),word(nWord)
{
}

DraggableWordObject::~DraggableWordObject()
{
}

void DraggableWordObject::onClicked()
{
	DragObject::onClicked();
}

void DraggableWordObject::onReleased()
{
	DragObject::onReleased();
}

void DraggableWordObject::update()
{
	DragObject::update();
}

void DraggableWordObject::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text(word, position.x, position.y + height/2.f);
	pop_settings();
}

string DraggableWordObject::getWord()
{
	return word;
}
