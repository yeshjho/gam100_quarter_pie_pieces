/*
	Interpreter.h

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Doyoon Kim
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

#include <deque>
#include <unordered_map>
#include <functional>
#include "Value.h"
#include "CodePiece.h"

namespace UserCodeInterpreter
{
	using CodePieceDeque = std::deque<CodePiece>;


	class Interpreter
	{
	public:
		Interpreter(const std::unordered_map<std::string, Value*>& variableDict,
			const std::unordered_map<std::string, std::function<void(const std::string&)>>& functionDict,
			const CodePieceDeque& codePieces);

		bool interpret();

		void setCodePieceStream(const CodePieceDeque& codePieces);

	private:
		CodePiece getCodePiece();
		void ungetCodePiece();

		// using Pratt algorithm to evaluate expressions
		double getNullDenotation(const CodePieceDeque& codePieces);
		double getLeftDenotation(const CodePieceDeque& codePieces, double lValue);
		double evaluate(const CodePieceDeque& codePieces, unsigned char rightBindingPower = 0, bool shouldResetIndex = false);

		CodePieceDeque getBlock();
		CodePieceDeque getUntilBoundary();
		CodePieceDeque getLine();
		void ignoreLine();


	private:
		std::unordered_map<std::string, Value*> variableDictionary;
		std::unordered_map<std::string, std::function<void(const std::string&)>> functionDictionary;
		CodePieceDeque codePieceStream;

		size_t codePieceIndex = 0;
		size_t codePieceStreamSize;

		size_t evaluateCodePieceIndex = 0;

		static constexpr unsigned char MAX_LOOP_COUNT = 100;
	};
}
