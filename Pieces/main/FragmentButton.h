/*
	FragmentButton.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ButtonObject.h"
using namespace std;

class FragmentButton : public ButtonObject
{
public:
	FragmentButton(const Position pos, float w,float h, char fragletter, int quantt, UIObject* parent);
	virtual ~FragmentButton() override;
	virtual void onClicked() override;
	virtual void onReleased() override;
	virtual void update() override;
	virtual void draw() override;
	void addQuantity(int n);
	void setQuantityTo(int n);

protected:
	
private:
	string fragment;
	char letter;
	int quantity = 0;
};


