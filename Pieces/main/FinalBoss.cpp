#include "FinalBoss.h"
#include "map.h"
#include "player.h"
#include <doodle/doodle.hpp>
#include "global.h"
#include "Bullet.h"

using namespace doodle;

FinalBoss::FinalBoss(const Vector& pos) : Object(FINAL_BOSS, pos)
{}

FinalBoss::FinalBoss(const Vector& pos, const Vector& spd) : Object(FINAL_BOSS, pos, spd)
{}

FinalBoss::~FinalBoss()
{}

void FinalBoss::update()
{
	switch (state)
	{
		case FinalBossState::DEAD:
			break;
		case FinalBossState::ALIVE:
			velocity += acceleration;
			position += velocity;
			acceleration.setXY(0.f, 0.f);
			between_blocks();
			addForce(Vector(0.f, GRAVITY));
			move();
			checkCollisionWithPlayer();
			if (final_boss_jump == false && is_on_ground)
			{
				check_is_cooldown();
			}
			if (can_give_damage == false)
			{
				cooldown_damage();
			}
			break;
	}
}

void FinalBoss::draw()
{
	switch (state)
	{
		case FinalBossState::DEAD:
			break;
		case FinalBossState::ALIVE:
			push_settings();
			set_rectangle_mode(RectMode::Center);
			set_fill_color(100, 200, 200);
			draw_rectangle(position.getX(), position.getY(), width, height);
			pop_settings();
			break;
	}
}

void FinalBoss::move()
{
	if (final_boss_jump)
	{
		Bullet* bullet = new Bullet(Vector(position.getX(), position.getY()), position.getX()>=player->getPos().getX());
		game.addObject(bullet);
		addForce(Vector(0.f, jump_value));
		addForce(Vector(moving_left_right, 0.f));
		check_current_cooldown = 0;
		is_on_ground = false;
		final_boss_jump = false;
	}
}

void FinalBoss::addForce(const Vector& force)
{
	acceleration += force.scale(1.f / mass);
}

void FinalBoss::between_blocks()
{
	for (int i = 0; i < blockPosition.size(); ++i)
	{
		//upper side
		if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y + BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
			is_on_ground = true;
		}
		//down side
		else if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2 && position.getY() < blockPosition[i].y - BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
		}
		//left side
		else if (position.getX() < blockPosition[i].x - BLOCK_WIDTH / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2);
			moving_left_right = -60.f;
		}
		//right side
		else if (position.getX() > blockPosition[i].x + BLOCK_WIDTH / 2 && position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2);
			moving_left_right = 60.f;
		}
	}
	//upper side
	if (position.getY() >= ((MAP_HEIGHT - 1) * BLOCK_HEIGHT) + 25)
	{
		position.setY((MAP_HEIGHT - 1) * BLOCK_HEIGHT + 25);
	}
	//down side
	else if (position.getY() <= (BLOCK_HEIGHT * (MAP_HEIGHT - 21)) + 25)
	{
		position.setY(BLOCK_HEIGHT * (MAP_HEIGHT - 21) + 25);
	}
	//left side
	else if (position.getX() <= (BLOCK_WIDTH * (MAP_WIDTH - 43)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 43) - 5);
	}
	//right side
	else if (position.getX() >= (BLOCK_WIDTH * (MAP_WIDTH - 1)) - 5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 1) - 5);
	}
}



void FinalBoss::checkCollisionWithPlayer()

{
	Vector player_position = player->getPos();
	if (sqrt(powf(position.getX() - player_position.getX(), 2.f) + powf(position.getY() - player_position.getY(), 2.f)) <= width/2+10 && can_give_damage)
	{
		player->takeDamage(damage);
		check_current_damage_cooldown = 0;
		can_give_damage = false;
	}
}

void FinalBoss::onDead()
{
	player->get_gameCleared(true);
	set_current_state(FinalBossState::DEAD);
}

void FinalBoss::check_is_cooldown()
{
	check_current_cooldown += DeltaTime;
	if (check_current_cooldown > 0.5f)
	{
		final_boss_jump = true;
	}
}

void FinalBoss::cooldown_damage()
{
	check_current_damage_cooldown += DeltaTime;
	if (check_current_damage_cooldown > 1.0f)
	{
		can_give_damage = true;
	}
}

void FinalBoss::set_current_state(FinalBossState State)
{
	state = State;
}

FinalBossState FinalBoss::get_current_state()
{
	return state;
}

void FinalBoss::set_health(int Health)
{
	health = Health;
}

int FinalBoss::get_health()
{
	return health;
}

float FinalBoss::get_width()
{
	return width;
}

void FinalBoss::set_jump_value(float jumpValue)
{
	jump_value = jumpValue;
}

void FinalBoss::setJumpValue(float jumpValue)
{
	jump_value = jumpValue;
}

void FinalBoss::set_damage(float Damage)
{
	damage = Damage;
}
