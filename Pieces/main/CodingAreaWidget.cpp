﻿#include "CodingAreaWidget.h"
#include "DraggableWordObject.h"
#include "HoldingWordsWidget.h"
#include "SingleCodePieces.h"
#include "global.h"
#include "constants.h"
using namespace UserCodeInterpreter;

CodingAreaWidget::CodingAreaWidget(const Position pos, float w, float h, UIObject* parent)
	:listWidget(pos, w, h, parent), runbutton({ pos.x + w / 2.f,pos.y }, w / 4.f, h / 24.f,static_cast<UIObject*>(this))
{
	ui.registerUIButtonObject(&runbutton);
}

CodingAreaWidget::~CodingAreaWidget()
{
}

void CodingAreaWidget::onDragObjectDropped(DragObject* obj)
{
	int type = obj->getType();
	if (type == DRAGGABLE_WORD_OBJECT)
	{
		DraggableWordObject* wordObj = static_cast<DraggableWordObject*>(obj);
		HoldingWordsWidget* parent = static_cast<HoldingWordsWidget*>(wordObj->getParent());
		parent->removeObjectFromList(obj);
		ui.removeUIDragObject(obj);

		string word = wordObj->getWord();
		char firstLetter = word[0];

		float codePieceWidth = width / 8.f;
		float codePieceHeight = height / 24.f;
		UIObject* thisAdress = static_cast<UIObject*>(this);
		DraggableCodePieceObject* codePiece = nullptr;
		//영단어
		if (isalpha(firstLetter))
		{
			if (word == "if")
			{
				codePiece = new CodePieceIf({ 0.f,0.f }, width, codePieceHeight, thisAdress, CONDITIONAL_PIECE, CodePiece(ECodeType::IF), word);
			}
			else if (word == "else")
			{
				codePiece = new CodePieceElse({ 0.f,0.f }, width, codePieceHeight, thisAdress, CONDITIONAL_PIECE, CodePiece(ECodeType::ELSE), word);
			}
			else if (word == "for")
			{
				codePiece = new CodePieceFor({ 0.f,0.f }, width, codePieceHeight, thisAdress, CONDITIONAL_PIECE, CodePiece(ECodeType::FOR), word);
			}
			else if (word == "while")
			{
				codePiece = new CodePieceWhile({ 0.f,0.f }, width, codePieceHeight, thisAdress, CONDITIONAL_PIECE, CodePiece(ECodeType::WHILE), word);
			}
			else if (word == "armor")
			{
				codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::CONSTANT, word), word);
			}
			else if(word == "sword")
			{
				codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::CONSTANT, word), word);
			}
			else if (word == "xarmor")
			{
				codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::CONSTANT, word), word);
			}
			else if (word == "xsword")
			{
				codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::CONSTANT, word), word);
			}
			else if (word == "giveitem")
			{
				codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::FUNCTION, word), word);
			}
			else
			{
				codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::VARIABLE,word), word);
			}
		}
		//숫자
		else if (isdigit(firstLetter))
		{
			codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::CONSTANT,atoi(word.c_str())), word);
		}
		//특수문자
		else
		{
			switch (firstLetter)
			{
				case '!':
					if (word == "!")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::NOT), word);
					}
					else if (word == "!=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::NOT_EQUAL), word);
					}
					break;
				case '&':
					codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::AND), word);
					break;
				case '|':
					codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::OR), word);
					break;
				case '+':
					if (word == "+")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::ADD), word);
					}
					else if (word == "+=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::ADD_IS), word);
					}
					break;
				case '-':
					if (word == "-")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::SUBTRACT), word);
					}
					else if (word == "-=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::SUBTRACT_IS), word);
					}
					break;
				case '%':
					if (word == "%")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::REMAINDER), word);
					}
					else if (word == "%=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::REMAINDER_IS), word);
					}
					break;
				case '/':
					if (word == "/")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::DIVIDE), word);
					}
					else if (word == "/=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::DIVIDE_IS), word);
					}
					break;
				case '*':
					if (word == "*")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::MULTIPLY), word);
					}
					else if (word == "*=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::MULTIPLY_IS), word);
					}
					break;
				case '=':
					if (word == "=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::IS), word);
					}
					else if (word == "==")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::EQUAL), word);
					}
					break;
				case '<':
					if (word == "<")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::LESSER), word);
					}
					else if (word == "<=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::LESSER_EQUAL), word);
					}
					break;
				case '>':
					if (word == ">")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::GREATER), word);
					}
					else if (word == ">=")
					{
						codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::GREATER_EQUAL), word);
					}
					break;
				case '(':
					codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::PARENTHESIS_OPEN), word);
					break;
				case ')':
					codePiece = new SingleCodePieces({ 0.f,0.f }, codePieceWidth, codePieceHeight, thisAdress, SINGLE_CODE_PIECE, CodePiece(ECodeType::PARENTHESIS_CLOSE), word);
					break;
			}
		}
		//위젯 돌면서 isinborder 체크하고 위젯에 등록해줌 어떤 위젯도 isInborder판정을 안내면 그냥 이 코딩에리아 위젯에 등록함.
		for (listWidget* codeWidget : codeWidgets)
		{
			if (codeWidget->isDragObjectInTheBorder(obj))
			{
				ui.registerUIDragObject(codePiece);
				codeWidget->addObjectToList(codePiece);
				delete obj;
				return;
			}
		}

		ui.registerUIDragObject(codePiece);
		this->addObjectToListFromHoldingWordsWidget(codePiece);
		delete obj;
	}
	else
	{
	    obj->setIsClicked(false);
		for (listWidget* codeWidget : codeWidgets)
		{
			if (codeWidget->isDragObjectInTheBorder(obj))
			{
				codeWidget->onDragObjectDropped(obj);
				return;
			}
		}

		listWidget* parentWidget = static_cast<listWidget*>(obj->getParent());
		parentWidget->removeObjectFromList(obj);
		addObjectToList(obj);
	}
}

void CodingAreaWidget::addObjectToList(UIObject* obj)
{
	listWidget::addObjectToList(obj);
	codePieces.push_back(static_cast<DraggableCodePieceObject*>(obj));
}

void CodingAreaWidget::addObjectToListFromHoldingWordsWidget(DraggableCodePieceObject* obj)
{
	listWidget::addObjectToList(obj);
	int type = static_cast<DragObject*>(obj)->getType();
	if (type == CONDITIONAL_PIECE)
	{
		static_cast<ConditionalPieces*>(obj)->RegisterWidgetsTo(this);
	}
	codePieces.push_back(static_cast<DraggableCodePieceObject*>(obj));
}

void CodingAreaWidget::removeObjectFromListByHoldingWordsWidget(DraggableCodePieceObject* obj)
{
	listWidget::removeObjectFromList(obj);
	int type = static_cast<DragObject*>(obj)->getType();

	if (type == CONDITIONAL_PIECE)
	{
		static_cast<ConditionalPieces*>(obj)->DeRegisterWidgetsFrom(this);
	}
	codePieces.remove(static_cast<DraggableCodePieceObject*>(obj));
}



void CodingAreaWidget::removeObjectFromList(UIObject* obj)
{
	listWidget::removeObjectFromList(obj);
	codePieces.remove(static_cast<DraggableCodePieceObject*>(obj));
}

void CodingAreaWidget::onClicked()
{
	
}

void CodingAreaWidget::onReleased()
{
	
}

void CodingAreaWidget::update()
{
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		codePiece->update();
	}
	for (listWidget* codeWidget : codeWidgets)
	{
		codeWidget->update();
	}
	runbutton.update();
}

void CodingAreaWidget::draw()
{
	push_settings();
	draw_rectangle(position.x, position.y, width, height);
	pop_settings();
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		codePiece->draw();
	}
	for (listWidget* codeWidget : codeWidgets)
	{
		codeWidget->draw();
	}
	runbutton.draw();
}

void CodingAreaWidget::alignObjects()
{
	float accumulatedHeight = 0.f;
	int columnCounter = 0;
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		if (codePiece->getType() == SINGLE_CODE_PIECE)
		{
			float objHeight = codePiece->getHeight();
			float newPosX = position.x + (float)columnCounter * codePiece->getWidth();
			float newPosY = position.y + height - objHeight - accumulatedHeight;
			codePiece->setPosXY(newPosX, newPosY);
			columnCounter = (columnCounter + 1) % MaxSingleCodePiecePerLine;
			if (columnCounter == 0)
				accumulatedHeight += objHeight;
		}
		else
		{
			if (columnCounter > 0)
			{
			  columnCounter = 0;
			  //singlePiece height.
			  accumulatedHeight += singleCodePieceHeight;
			}
			float objHeight = codePiece->getHeight();
			float newPosY = position.y + height - objHeight - accumulatedHeight;
			codePiece->setPosXY(position.x, newPosY);
			codePiece->alignObjects();
			accumulatedHeight += objHeight;
		}
	}
}

void CodingAreaWidget::interPret()
{
	vector<CodePiece> CodePieces;
	int endLineCounter = 0;
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		int codeType = codePiece->getType();
		const vector<CodePiece>& tmp = codePiece->getInterpreterCodePieces();
		CodePieces.insert(CodePieces.end(), tmp.begin(), tmp.end());
		if (codeType == SINGLE_CODE_PIECE)
		{
			endLineCounter++;
			if (endLineCounter == MaxSingleCodePiecePerLine)
			{
				CodePieces.push_back(CodePiece(ECodeType::END_LINE));
				endLineCounter = 0;
			}
		}
		else
		{
			endLineCounter = 0;
		}
	}
	CodePieces.push_back(CodePiece(ECodeType::PROGRAM_END));

	game.runInterpreter(CodePieces);
	reset();
}

void CodingAreaWidget::reset()
{
	for (DraggableCodePieceObject* codePiece : codePieces)
	{
		if (codePiece != nullptr)
		{
			ui.removeUIDragObject(codePiece);
			delete codePiece;
		}
	}

	codePieces.clear();
	codeWidgets.clear();
}

	
	



