#include "ButtonObject.h"
#include "global.h"

ButtonObject::ButtonObject(const Position pos,float w,float h,UIObject* parent)
	:UIObject(pos,w,h),pParent(parent)
{

}

ButtonObject::~ButtonObject()
{
}

void ButtonObject::onClicked()
{
	isClicked = true;
}

void ButtonObject::onReleased()
{
	isClicked = false;
}

void ButtonObject::update()
{

}

UIObject* ButtonObject::getParent() const
{
	return pParent;
}
