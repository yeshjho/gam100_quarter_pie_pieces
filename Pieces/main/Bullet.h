/*
	Bullet.h

	GAM100, Fall 2019

	JoonHo Hwang Wrote this all
	Doyoon Kim designed the structure
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
class Bullet : public Object
{
public:
	Bullet(const Vector& pos, bool isGoingLeft);
	~Bullet() override;

	virtual void update() override;
	virtual void draw() override;
	virtual void move() override;

	virtual void between_blocks() override {}
	virtual void checkCollisionWithPlayer() override;
	virtual void onDead() override;

	virtual void addForce(const Vector&) override {}

private:
	static constexpr float width = 10;
	static constexpr float height = 10;

	static constexpr float velocityX = 10;

	static constexpr float damage = 2;

	bool isDestroyed = false;
};

