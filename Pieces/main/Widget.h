/*
	Widget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "UIObject.h"

class Widget : public UIObject
{
public:
	Widget(const Position pos, float w, float h ,UIObject* parent);
	virtual ~Widget() = 0;
	//align objects
	bool isDragObjectInTheBorder(UIObject* obj);
	virtual UIObject* getParent();
protected:
	UIObject* pParent;
private:
};

