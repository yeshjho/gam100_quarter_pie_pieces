#include "CodePiece.h"

using namespace UserCodeInterpreter;


CodePiece::CodePiece(const ECodeType type) : type(type)
{
	/*************
	PRECEDENCE

	! + - (unary)
	* / %
	+ -
	< <= > >=
	== !=
	AND
	OR
	*************/
	switch (type)
	{
		case ECodeType::AND:
			leftBindingPower = 2;
			break;

		case ECodeType::OR:
			leftBindingPower = 1;
			break;

		case ECodeType::NOT:
			leftBindingPower = 7;
			break;

		case ECodeType::ADD:
			leftBindingPower = 5;
			break;

		case ECodeType::SUBTRACT:
			leftBindingPower = 5;
			break;

		case ECodeType::MULTIPLY:
			leftBindingPower = 6;
			break;

		case ECodeType::DIVIDE:
			leftBindingPower = 6;
			break;

		case ECodeType::REMAINDER:
			leftBindingPower = 6;
			break;

		case ECodeType::NOT_EQUAL:
			leftBindingPower = 3;
			break;

		case ECodeType::EQUAL:
			leftBindingPower = 3;
			break;

		case ECodeType::GREATER:
			leftBindingPower = 4;
			break;

		case ECodeType::GREATER_EQUAL:
			leftBindingPower = 4;
			break;

		case ECodeType::LESSER:
			leftBindingPower = 4;
			break;

		case ECodeType::LESSER_EQUAL:
			leftBindingPower = 4;
			break;

		default:
			break;
	}
}

Value UserCodeInterpreter::CodePiece::getValue() const
{
	if (value.valueless_by_exception())
	{
		throw "the value is empty";
	}

	return value;
}

ECodeType UserCodeInterpreter::CodePiece::getType() const
{
	return type;
}

unsigned char UserCodeInterpreter::CodePiece::getLBP() const
{
	return leftBindingPower;
}


