/*
	listWidget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Widget.h"
#include "DragObj.h"
using namespace std;

class listWidget : public Widget
{
public:
	listWidget(const Position pos, float w, float h, UIObject* parent);
	virtual ~listWidget() = 0;
	virtual void addObjectToList(UIObject* obj);
	virtual void removeObjectFromList(UIObject* obj);
	virtual void onDragObjectDropped(DragObject* obj) = 0;
	virtual void alignObjects();
	virtual void resize(float w, float h)override;
protected:
	list<UIObject*> listedObjects;
};
