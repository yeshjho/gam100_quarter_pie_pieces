#include "UIObject.h"
using namespace doodle;

UIObject::UIObject(const Position pos, float w, float h)
	:position(pos),width(w),height(h)
{
}

UIObject::~UIObject()
{
}

bool UIObject::checkIsMouseInTheBorder()const
{
	float m_x = (float)get_mouse_x();
	float m_y = (float)get_mouse_y();

	return (m_x > position.x && m_x < position.x + width) && (m_y > position.y && m_y < position.y + height);
}

bool UIObject::getIsClicked()const
{
	return isClicked;
}

void UIObject::setIsClicked(bool isclicked)
{
	isClicked = isclicked;
}

void UIObject::resize(float w, float h)
{
	width = w;
	height = h;
}

void UIObject::setPosXY(float n_x, float n_y)
{
	position.x = n_x;
	position.y = n_y;
}

void UIObject::setPosX(float n_x)
{
	position.x = n_x;
}

void UIObject::setPosY(float n_y)
{
	position.y = n_y;
}

Position UIObject::getPosXY() const
{
	return position;
}

float UIObject::getPosX() const
{
	return position.x;
}

float UIObject::getPosY() const
{
	return position.y;
}

float UIObject::getWidth() const
{
	return width;
}

float UIObject::getHeight() const
{
	return height;
}
