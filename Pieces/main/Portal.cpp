#include "Portal.h"
#include "global.h"
#include "doodle/doodle.hpp"

using namespace doodle;

Portal::Portal(const Vector& pos) : Object(PORTAL, pos) {}

Portal::~Portal() {}

void Portal::update()
{
	checkCollisionWithPlayer();
}

void Portal::checkCollisionWithPlayer()
{
	const Vector player_position = player->getPos();
	const float x = position.getX();
	const float y = position.getY();
	
	if (powf(x - player_position.getX(), 2.f) + powf(y - player_position.getY(), 2.f) <= powf(width, 2.f))
	{
		canBeActivated = true;
	}
	else
	{
		canBeActivated = false;
	}
}

void Portal::draw()
{
	push_settings();
	set_rectangle_mode(RectMode::Center);
	no_outline();
	set_fill_color(255, 127, 255, 100);
	draw_rectangle(position.getX(), position.getY(), width, height);
	pop_settings();
}

void Portal::activate() const
{
	if (!canBeActivated)
	{
		return;
	}

	unsigned char mapNumber = game.getCurrentMapNumber();
	const float playerX = player->getPos().getX();
	if (playerX < Width / 3)
	{
		mapNumber--;
	}
	else if (Width / 3 <= playerX && playerX <= Width / 2)
	{
		mapNumber = BOSS_ROOM_NUMBER;
	}
	else
	{
		mapNumber++;
	}
	game.isPendingNewMap = true;
	game.pendingMapNumber = mapNumber;
}