#include "ConditionalPieces.h"

ConditionalPieces::ConditionalPieces(Position pos, float w, float h, UIObject* parent, int type, const CodePiece& rCodeType, const string& nWord)
	:DraggableCodePieceObject(pos, w, h, parent, type, rCodeType, nWord),
	codeBodyWidget({ pos.x,pos.y - h }, w, h, parent)
{}

ConditionalPieces::~ConditionalPieces()
{
}

void ConditionalPieces::setPosXY(float n_x, float n_y)
{
	position.x = n_x;
	position.y = n_y + codeBodyWidget.getHeight();
}

