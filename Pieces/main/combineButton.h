/*
	combineButton.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ButtonObject.h"
class CombineButton : public ButtonObject
{
public:
	CombineButton(const Position pos, float w, float h, UIObject* parent);
	virtual ~CombineButton() override;
	virtual void onClicked() override;
	virtual void onReleased() override;
	virtual void update() override;
	virtual void draw() override;
private:
};

