/*
	DragObject.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "UIObject.h"

class DragObject : public UIObject
{
public:
	DragObject(const Position pos,float w,float h,UIObject* parent,int nType);
	virtual ~DragObject() = 0;
	virtual void onClicked() override;
	virtual void onReleased() override;
	virtual void update() override;
	virtual void draw() = 0;
	int getType()const;
	virtual UIObject* getParent();
	void setParent(UIObject* parent);
private:
	UIObject* pParent;
	int type;
};

