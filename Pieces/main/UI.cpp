﻿#include "UI.h"
#include "global.h"
#include <iostream>

UI::UI(const Position pos, float w, float h)
	:UIObject(pos, w, h),
	fragmentsWidget({ pos.x,pos.y+h*(2.f/3.f) },w/2.f,h/3.f,static_cast<UIObject*>(this)),
	wordmakingareaWidget({pos.x,pos.y+h/3.f},w/2.f,h/3.f,static_cast<UIObject*>(this)),
	holdingwordsWidget({ pos.x,pos.y },w/2.f,h/3.f,static_cast<UIObject*>(this)),
	codingAreaWidget({pos.x+w/2.f,pos.y}, w / 2.f, h, static_cast<UIObject*>(this))
{
}

UI::~UI()
{
}

void UI::draw()
{
	switch(gameState)
	{
	case STATE::MAIN_MENU:
		break;
	case STATE::IN_GAME:
	{
		push_settings();
		set_rectangle_mode(RectMode::Corner);
		set_fill_color(255);
		draw_rectangle(50, 50, Width / 2.f, Height / 20.f);
		set_fill_color(255, 0, 0);
		const float healthPortion = player->getHealth() / Player::MAX_HEALTH < 0 ? 0 : player->getHealth() / Player::MAX_HEALTH;
		draw_rectangle(50, 50, Width / 2.f * healthPortion, Height / 20.f);
		pop_settings();
		break;
	}
	case STATE::CODING:
		codingAreaWidget.draw();
		fragmentsWidget.draw();
		wordmakingareaWidget.draw();
		holdingwordsWidget.draw();
	
		if(currentDraggingObject != nullptr)
			currentDraggingObject->draw();
		break;
	default: break;
	}
}

void UI::update()
{
	switch (gameState)
	{
	case STATE::MAIN_MENU:
		break;
	case STATE::IN_GAME:
		break;
	case STATE::CODING:
		fragmentsWidget.update();
		wordmakingareaWidget.update();
		holdingwordsWidget.update();
		codingAreaWidget.update();
		break;
	default: 
		break;
	}
}

void UI::onClicked()
{
}

void UI::onReleased()
{
}


void UI::addFragment(char fragLetter, int n)
{
	FragmentButton* fragButton = fragmentsTable[fragLetter];
	fragButton->addQuantity(n);
}

void UI::addToBuffer(char fragLetter)
{
	wordmakingareaWidget.addToBuffer(fragLetter);
}

void UI::addToHoldingWords(const string& word)
{
	holdingwordsWidget.addHoldingWord(word);
}

void UI::onMouseClicked()
{
	for (ButtonObject* obj : UIButtonObjects)
	{
		if (obj->checkIsMouseInTheBorder())
		{
			obj->onClicked();
			break;
		}
	}
	for (DragObject* obj : UIDragObject)
	{
		if (obj->checkIsMouseInTheBorder())
		{
			obj->onClicked();
			currentDraggingObject = obj;
			break;
		}
	}
}

void UI::onMouseReleased()
{
	for (ButtonObject* obj : UIButtonObjects)
	{
		if (obj->checkIsMouseInTheBorder())
		{
			obj->onReleased();
			break;
		}
	}

	if (currentDraggingObject == nullptr)
		return;

	if (wordmakingareaWidget.isDragObjectInTheBorder(currentDraggingObject))
	{
		wordmakingareaWidget.onDragObjectDropped(currentDraggingObject);
		currentDraggingObject = nullptr;
		alignObjects();
		return;
	}
	else if (holdingwordsWidget.isDragObjectInTheBorder(currentDraggingObject))
	{
		holdingwordsWidget.onDragObjectDropped(currentDraggingObject);
		currentDraggingObject = nullptr;
		alignObjects();
		return;
	}
	else if (codingAreaWidget.isDragObjectInTheBorder(currentDraggingObject))
	{
		codingAreaWidget.onDragObjectDropped(currentDraggingObject);
		currentDraggingObject = nullptr;
		alignObjects();
		return;
	}
	else
	{
		currentDraggingObject->setIsClicked(false);
		currentDraggingObject = nullptr;
		alignObjects();
	}
}

void UI::registerUIButtonObject(ButtonObject* obj)
{
	UIButtonObjects.push_back(obj);
}

void UI::registerUIDragObject(DragObject* obj)
{
	UIDragObject.push_back(obj);
}

void UI::removeUIButtonObject(ButtonObject* obj)
{
	UIButtonObjects.remove(obj);
}

void UI::removeUIDragObject(DragObject* obj)
{
	UIDragObject.remove(obj);
}

void UI::reset()
{
	for (auto it = fragmentsTable.begin(); it != fragmentsTable.end(); it++)
	{
		FragmentButton* fragButton = it->second;
		fragButton->setQuantityTo(initialFragmentQuantity);
	}
	wordmakingareaWidget.reset();
	holdingwordsWidget.reset();
	codingAreaWidget.reset();
}

void UI::alignObjects()
{
	codingAreaWidget.alignObjects();
	holdingwordsWidget.alignObjects();
}
