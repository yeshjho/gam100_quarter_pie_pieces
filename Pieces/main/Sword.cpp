#include "Sword.h"
#include "global.h"

Sword::Sword(SwordType sword_type) : type(sword_type), damage(swordDamage[sword_type]), color(swordColor[sword_type]) {}

void Sword::give_sword()
{
	int get_rand_value = random(0, 100) + 1;
	Sword new_sword = { SwordType::NORMAL_SWORD };


	if(get_rand_value > 20 && get_rand_value <=90)
	{
		new_sword = { SwordType::RARE_SWORD };
	}
	else if(get_rand_value <=99)
	{
		new_sword = { SwordType::EPIC_SWORD };
	}
	else
	{
		new_sword = { SwordType::LEGENDARY_SWORD };
	}
	player->getSword(new_sword);
}

void Sword::give_xsword()
{
	player->getSword(SwordType::EPIC_SWORD);
}


int Sword::get_damage() const
{
	return damage;
}

int Sword::get_color() const
{
	return color;
}
