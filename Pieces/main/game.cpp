#include  "game.h"
#include  "common.h"
#include "map.h"
#include "MiddleBoss.h"
#include "global.h"
#include "Interpreter.h"

using namespace UserCodeInterpreter;

Game::Game()
{

}

Game::~Game()
{
	clearObjects();
}


void Game::update()
{
	switch (gameState)
	{
	case STATE::MAIN_MENU:
		break;

	case STATE::IN_GAME:
		for (Object* obj : objects)
		{
			obj->update();
		}
		if (!pendingObjects.empty())
		{
			objects.insert(objects.end(), pendingObjects.begin(), pendingObjects.end());
			pendingObjects.clear();
		}
		// loading new map causes deleting objects, thus separating from the loop above
		if (isPendingNewMap)
		{
			loadMap(pendingMapNumber, currentMapNumber < pendingMapNumber);
			isPendingNewMap = false;
		}
		break;
	case STATE::CODING:
		break;
	default:
		break;
	}
}

void Game::draw() const
{
	switch(gameState)
	{
	case STATE::MAIN_MENU :
		push_settings();
		set_frame_of_reference(RightHanded_OriginCenter);
		set_font_size(50);
		set_fill_color(127, 255, 127);
		draw_text(GAME_TITLE, -100, 150);

		set_font_size(25);
		set_fill_color(255);
		draw_text("Press S to Start the game", -200, 0);
		draw_text("Press Alt+F4 to Quit the game", -200, -200);
		pop_settings();
		break;
	case STATE::IN_GAME :
		draw_map();
	    for (Object* obj : objects)
	    {
		    obj->draw();
	    }
		break;
	case STATE::CODING :
		break;
	default:
		break;
	}
}

unsigned char Game::getCurrentMapNumber() const
{
	return currentMapNumber;
}

void Game::addObject(Object* object)
{
	pendingObjects.push_back(object);
}

void Game::loadMap(unsigned char roomNum, bool isPlayerPositionLeft)
{
	currentMapNumber = roomNum;

	clearObjects();
	blockPosition.clear();
	vector<Vector> playerPositionCandidates;

	for (int row = 0; row < MAP_HEIGHT; row++)
	{
		for (int col = 0; col < MAP_WIDTH; col++)
		{
			float posX = static_cast<float>(30 * col);
			float posY = static_cast<float>(30 * (25 - row));
			switch (map[roomNum - 1][row][col])
			{
			case 0:
				//nothing
				break;
			case 1:
				//blocks
				blockPosition.push_back({ posX,posY });
				break;
			case 2:
			{
				//item box image
				Chest* chest = new Chest(Vector(posX, posY));
				objects.push_back(chest);
				break;
			}
			case 3:
			{
				//set spawn point
				playerPositionCandidates.push_back(Vector(posX, posY));
				break;
			}
			case 4:
			{
				Crab* crab = new Crab(Vector(posX, posY));
				objects.push_back(crab);
				break;
			}
			case 5:
			{
				Frog* frog = new Frog(Vector(posX, posY));
				objects.push_back(frog);
				break;
			}
			case 6:
			{
				Portal* portal = new Portal(Vector(posX, posY));
				objects.push_back(portal);
				break;
			}
			case 7:
			{
				MiddleBoss* middleboss = new MiddleBoss(Vector(posX, posY));
				objects.push_back(middleboss);
				break;
			}
			case 8:
				FinalBoss * finalboss = new FinalBoss(Vector(posX, posY));
				objects.push_back(finalboss);
				break;
			}
		}
	}
	if (!playerPositionCandidates.empty())
	{
		sort(playerPositionCandidates.begin(), playerPositionCandidates.end(), [](const Vector& a, const Vector& b) { return a.getX() < b.getX(); });
		player->setPos(isPlayerPositionLeft ? playerPositionCandidates[0] : playerPositionCandidates.back());
	}
}

vector<Object*> Game::getObjects()
{
	return objects;
}


void Game::clearObjects()
{
	for (Object* obj : objects)
	{
		if (obj != nullptr && obj->getType() != PLAYER)
		{
			delete obj;
		}
	}
	objects.clear();
	objects.push_back(player);
}

void Game::runInterpreter(const vector<UserCodeInterpreter::CodePiece>& codePieces)
{
	Value playerJump{player->getJumpValue(), false};
	Value playerHealth{player->getHealth(), false};
	Value playerSpeed{player->getSpeed(), false};

	Value crabDamage{1, false};
	Value crabHealth{1, false};

	Value frogDamage{1.5, false};
	Value frogJump{70, false};
	Value frogHealth{2, false};

	Value xDamage{3, false};
	Value xJump{70, false};
	Value xHealth{15, false};

	Value xyzDamage{3, false};
	Value xyzJump{70, false};
	Value xyzHealth{20, false};

	Value i;

	Interpreter* interpreter = new Interpreter{
		{
			{"playerjump", &playerJump}, {"playerhealth", &playerHealth}, {"playerspeed", &playerSpeed},
			{"crabdamage", &crabDamage}, {"crabhealth", &crabHealth},
			{"frogdamage", &frogDamage}, {"frogjump", &frogJump}, {"froghealth", &frogHealth},
			{"xdamage", &xDamage}, {"xjump", &xJump}, {"xhealth", &xHealth},
			{"xyzdamage", &xyzDamage}, {"xyzjump", &xyzJump}, {"xyzhealth", &xyzHealth},
			{"i", &i}
		},
		{{"giveitem", &give_item}},
		{codePieces.begin(), codePieces.end()}
	};
	if (!interpreter->interpret())
	{
		// error
	}

	if (playerJump.getIsHoldingValue())
	{
		player->setJumpValue(static_cast<float>(get<double>(playerJump)));
	}
	if (playerHealth.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(playerHealth));
		if (newValue <= 0)
		{
			player->onDead();
		}
		else
		{
			player->setHealth(newValue);
		}
	}
	if (playerSpeed.getIsHoldingValue())
	{
		player->setSpeed(static_cast<float>(get<double>(playerSpeed)));
	}

	if (crabDamage.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(crabDamage));

		for (Object* object : objects)
		{
			if (object->getType() == CRAB)
			{
				((Crab*)object)->set_damage(newValue);
			}
		}
	}
	if (crabHealth.getIsHoldingValue())
	{
		const int newValue = static_cast<int>(get<double>(crabHealth));

		if (newValue <= 0)
		{
			for (Object* object : objects)
			{
				if (object->getType() == CRAB)
				{
					object->onDead();
				}
			}
		}
		else
		{
			for (Object* object : objects)
			{
				if (object->getType() == CRAB)
				{
					((Crab*)object)->set_health(newValue);
				}
			}
		}
	}

	if (frogDamage.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(frogDamage));

		for (Object* object : objects)
		{
			if (object->getType() == FROG)
			{
				((Frog*)object)->set_damage(newValue);
			}
		}
	}
	if (frogHealth.getIsHoldingValue())
	{
		const int newValue = static_cast<int>(get<double>(frogHealth));

		if (newValue <= 0)
		{
			for (Object* object : objects)
			{
				if (object->getType() == FROG)
				{
					object->onDead();
				}
			}
		}
		else
		{
			for (Object* object : objects)
			{
				if (object->getType() == FROG)
				{
					((Frog*)object)->set_health(newValue);
				}
			}
		}
	}
	if (frogJump.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(frogJump));

		for (Object* object : objects)
		{
			if (object->getType() == FROG)
			{
				((Frog*)object)->setJumpValue(newValue);
			}
		}
	}

	if (xDamage.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(xDamage));

		for (Object* object : objects)
		{
			if (object->getType() == MIDDLE_BOSS)
			{
				((MiddleBoss*)object)->set_damage(newValue);
				break;
			}
		}
	}
	if (xHealth.getIsHoldingValue())
	{
		const int newValue = static_cast<int>(get<double>(xHealth));

		if (newValue <= 0)
		{
			for (Object* object : objects)
			{
				if (object->getType() == MIDDLE_BOSS)
				{
					object->onDead();
					break;
				}
			}
		}
		else
		{
			for (Object* object : objects)
			{
				if (object->getType() == MIDDLE_BOSS)
				{
					((MiddleBoss*)object)->set_health(newValue);
					break;
				}
			}
		}
	}
	if (xJump.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(xJump));

		for (Object* object : objects)
		{
			if (object->getType() == MIDDLE_BOSS)
			{
				((MiddleBoss*)object)->setJumpValue(newValue);
				break;
			}
		}
	}

	if (xyzDamage.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(xyzDamage));

		for (Object* object : objects)
		{
			if (object->getType() == FINAL_BOSS)
			{
				((FinalBoss*)object)->set_damage(newValue);
				break;
			}
		}
	}
	if (xyzHealth.getIsHoldingValue())
	{
		const double newValue = get<double>(xyzHealth);

		if (newValue <= 0)
		{
			for (Object* object : objects)
			{
				if (object->getType() == FINAL_BOSS)
				{
					object->onDead();
					break;
				}
			}
		}
		else
		{
			for (Object* object : objects)
			{
				if (object->getType() == FINAL_BOSS)
				{
					((FinalBoss*)object)->set_health(static_cast<int>(newValue));
					break;
				}
			}
		}
	}
	if (xyzJump.getIsHoldingValue())
	{
		const float newValue = static_cast<float>(get<double>(xyzJump));

		for (Object* object : objects)
		{
			if (object->getType() == FINAL_BOSS)
			{
				((FinalBoss*)object)->setJumpValue(newValue);
				break;
			}
		}
	}

	delete interpreter;
}