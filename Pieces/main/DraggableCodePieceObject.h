/*
	DraggableCodePieceObject.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "DragObj.h"
#include "CodePiece.h"
using namespace UserCodeInterpreter;
using namespace std;

class DraggableCodePieceObject : public DragObject
{
public:
	DraggableCodePieceObject(const Position pos, float w, float h,UIObject* parent,int type,const CodePiece& rCodeType,const string& nWord);
	virtual ~DraggableCodePieceObject() = 0;
	virtual void onClicked() override;
	virtual void onReleased() override;
	virtual void draw() = 0;
	virtual void alignObjects() = 0;
	virtual void resize(float w, float h);
	string getWord()const;
	virtual vector<string> getCodePiecesToWords() = 0;
	virtual vector<CodePiece> getInterpreterCodePieces() = 0;
protected:
	CodePiece codeType;
	string word;
};

