/*
	CodeBodyWidget.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim Wrote this all
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "listWidget.h"
#include "DraggableCodePieceObject.h"

class CodeBodyWidget : public listWidget
{
public:
	CodeBodyWidget(Position pos, float w, float h, UIObject* parent);
	virtual ~CodeBodyWidget();
	virtual void onClicked()override;
	virtual void onReleased()override;
	virtual void update()override;
	virtual void draw()override;
	virtual void onDragObjectDropped(DragObject* obj)override;
	virtual void addObjectToList(UIObject* obj);
	virtual void removeObjectFromList(UIObject* obj);
	virtual void alignObjects()override;
	virtual float getHeight() const override;
	void autoResize();
	vector<string> getCodePiecesToWords();
	vector<CodePiece> getInterpreterCodePieces();
private:
	list<DraggableCodePieceObject*> codePieces;
};

