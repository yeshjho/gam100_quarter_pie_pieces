#include "RunButton.h"
#include "CodingAreaWidget.h"
#include "constants.h"
using namespace doodle;

RunButton::RunButton(const Position pos, float w, float h, UIObject* parent)
	:ButtonObject(pos,w,h,parent)
{
	
}

RunButton::~RunButton()
{
}

void RunButton::onClicked()
{
	ButtonObject::onClicked();
	CodingAreaWidget* parent = static_cast<CodingAreaWidget*>(getParent());
	parent->interPret();
}

void RunButton::onReleased()
{
	ButtonObject::onReleased();
}

void RunButton::update()
{
	ButtonObject::update();
}

void RunButton::draw()
{
	push_settings();
	set_font_size(UITextSize);
	draw_rectangle(position.x, position.y, width, height);
	draw_text("Run ", position.x + 5.f, position.y);
	pop_settings();
}
