/*
	Armor.h

	GAM100, Fall 2019

	JoonHo Hwang Wrote this
	Doyoon Kim
	Junhyuk Cha

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

class Armor
{
public:
	Armor(unsigned char tier);

	static void give_armor();
	static void give_xarmor();
	float getDamageReductionPercentage() const;
	unsigned char getTier() const;

private:
	unsigned char tier;
	float damageReductionPercentage;

	static const int MAX_TIER = 4;
	inline static const float damageReductionPerTier[] = { 10.f, 20.f, 35.f, 50.f };
};
