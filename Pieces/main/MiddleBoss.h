/*
	MiddleBoss.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim designed the structure
	Junhyuk Cha implemented

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "object.h"

enum class MiddleBossState
{
	DEAD, ALIVE
};

class MiddleBoss : public Object
{
public:
	MiddleBoss(const Vector& pos);
	MiddleBoss(const Vector& pos, const Vector& spd);
	~MiddleBoss() override;
	void update() override;
	void draw() override;
	void move() override;
	void addForce(const Vector& force) override;
	void between_blocks() override;

	void check_is_cooldown();
	void checkCollisionWithPlayer() override;
	void onDead() override;

	void cooldown_damage();
	void set_current_state(MiddleBossState State);
	MiddleBossState get_current_state();
	void set_health(int Health);
	int get_health();
	float get_width();
	void set_damage(float Damage);

	void setJumpValue(float jumpValue);

private:
	float mass = 10.f;
	float width = 60.f;
	float height = 60.f;
	float jump_value = 70.f;
	bool middle_boss_jump = false;
	bool is_on_ground = false;
	float moving_left_right = 60.f;
	float check_current_cooldown = 0;
	float check_current_damage_cooldown = 0;
	bool can_give_damage = true;
	MiddleBossState state = MiddleBossState::ALIVE;
	int health = 15;
	float damage = 3;
};