/*
	crab.h

	GAM100, Fall 2019

	JoonHo Hwang
	Doyoon Kim designed the structure
	Junhyuk Cha implemented

	All content (C) 2019 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

enum class CrabState
{
	DEAD, ALIVE
};

class Crab : public Object
{
public:
	Crab(const Vector& pos);
	Crab(const Vector& pos, const Vector& spd);
	~Crab() override;
	void update() override;
	void draw() override;
	void move() override;
	void addForce(const Vector& force) override;
	void cooldown_damage();
	void set_current_state(CrabState state);
	CrabState get_current_state();
	void between_blocks() override;
	void checkCollisionWithPlayer() override;
	void onDead() override;
	void set_damage(float Damage);
	void set_health(int Health);
	int get_health();

private:
	float mass = 10.f;
	float width = 20.f;
	float height = 20.f;
	float moving_left_right = 2.f;
	float check_current_cooldown = 0;
	bool can_give_damage = true;
	CrabState state = CrabState::ALIVE;
	float damage = 1;
	int health = 1;
};


