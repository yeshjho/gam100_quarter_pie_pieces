﻿#include "HoldingWordsWidget.h"
#include "DraggableWordObject.h"
#include "global.h"

HoldingWordsWidget::HoldingWordsWidget(const Position pos, float w, float h, UIObject* parent)
	:listWidget(pos,w,h,parent)
{
}

HoldingWordsWidget::~HoldingWordsWidget()
{
}

void HoldingWordsWidget::onClicked()
{
}

void HoldingWordsWidget::onReleased()
{
}

void HoldingWordsWidget::update()
{
	for (UIObject* obj : listedObjects)
	{
		obj->update();
	}
}

void HoldingWordsWidget::draw()
{
	push_settings();
	draw_rectangle(position.x, position.y, width, height);
	pop_settings();
	for (UIObject* obj : listedObjects)
	{
		obj->draw();
	}
}


void HoldingWordsWidget::onDragObjectDropped(DragObject* obj)
{
	obj->setIsClicked(false);
}

void HoldingWordsWidget::addHoldingWord(const string& word)
{
	DraggableWordObject* wordObj = new DraggableWordObject({ position.x,0.f }, width/2.f, height / 6.f, static_cast<UIObject*>(this), word, DRAGGABLE_WORD_OBJECT);
	listedObjects.push_back(wordObj);
	ui.registerUIDragObject(wordObj);
	alignObjects();
}

void HoldingWordsWidget::reset()
{
	for (UIObject* obj : listedObjects)
	{
		ui.removeUIDragObject(static_cast<DragObject*>(obj));
		delete obj;
	}
	listedObjects.clear();
}
