#include "player.h"
#include "map.h"
#include "global.h"

Player::Player(const Vector& pos)
	:Object(PLAYER,pos)
{
}

Player::Player(const Vector& pos, const Vector& spd)
	:Object(PLAYER,pos,spd)
{
}

Player::~Player()
{
	
}

void Player::update()
{
	addForce(Vector(0.f, GRAVITY));
	velocity += acceleration;
	position += velocity;
	move();
	acceleration.setXY(0.f, 0.f);
	between_blocks();
	if (can_slash == false)
	{
		check_is_cooldown();
	}
	attack();
	if(game_cleared)
	{
		push_settings();
		set_outline_color(250, 250, 250);
		draw_text("Game Cleared!!!", 170, 600);
		draw_text("Press ALT+F4 to exit the game", 170, 500);
		pop_settings();
	}
}

void Player::draw()
{
	push_settings();
	set_rectangle_mode(RectMode::Center);
	set_fill_color(HexColor(armor_color));
	draw_rectangle(position.getX(), position.getY(), width, height);
	no_outline();

	if(getSeeLeft())
	{
		if(is_slash)
		{
			set_fill_color(HexColor{current_sword.get_color()});
			draw_rectangle(position.getX()-width/2-HITBOX_WIDTH/2, position.getY(), HITBOX_WIDTH, HITBOX_HEIGHT);
			can_slash = false;
			check_current_cooldown = 0;
			is_slash = false;
		}
	}
	else
	{
		if (is_slash)
		{
			set_fill_color(HexColor{ current_sword.get_color() });
			draw_rectangle(position.getX() + width / 2 + HITBOX_WIDTH / 2, position.getY(), HITBOX_WIDTH, HITBOX_HEIGHT);
			can_slash = false;
			check_current_cooldown = 0;
			is_slash = false;
		}
	}
	pop_settings();
}

void Player::move()
{
		if (is_left)
		{
			position += Vector(-speed, 0.f);
		}
		if (is_right)
		{
			position += Vector(speed, 0.f);
		}
		if(is_jump)
		{
			can_jump = false;
		}
}

void Player::jump()
{
	addForce(Vector(0.f, jump_value));
	is_jump = true;
}

void Player::attack()
{
	if (!is_slash)
	{
		return;
	}
	for (Object* obj : game.getObjects())
	{
		switch(obj->getType())
		{
			case CRAB:
			{
				if(((Crab*)obj)->get_current_state() == CrabState::DEAD)
				{
					continue;
				}
				Vector crab_position = obj->getPos();
				if (getSeeLeft() && sqrt(powf((position.getX() - width / 2 - HITBOX_WIDTH / 2) - crab_position.getX(), 2)
					+ powf(position.getY() - crab_position.getY(), 2)) < (HITBOX_WIDTH / 2 + width / 2))
				{
					((Crab*)obj)->set_health(((Crab*)obj)->get_health() - current_sword.get_damage());
				}
				else if (!getSeeLeft() && sqrt(powf((position.getX() + width / 2 + HITBOX_WIDTH / 2) - crab_position.getX(), 2)
					+ powf(position.getY() - crab_position.getY(), 2)) < (HITBOX_WIDTH / 2 + width / 2))
				{
					((Crab*)obj)->set_health(((Crab*)obj)->get_health() - current_sword.get_damage());
				}
				if (((Crab*)obj)->get_health() <= 0)
				{
					((Crab*)obj)->onDead();
				}
				break;
			}
			case FROG:
			{
				if (((Frog*)obj)->get_current_state() == FrogState::DEAD)
				{
					continue;
				}
				Vector frog_position = obj->getPos();
				if (getSeeLeft() && sqrt(powf((position.getX() - width / 2 - HITBOX_WIDTH / 2) - frog_position.getX(), 2)
					+ powf(position.getY() - frog_position.getY(), 2)) < (HITBOX_WIDTH / 2 + width / 2))
				{
					((Frog*)obj)->set_health(((Frog*)obj)->get_health() - current_sword.get_damage());
				}
				else if (!getSeeLeft() && sqrt(powf((position.getX() + width / 2 + HITBOX_WIDTH / 2) - frog_position.getX(), 2)
					+ powf(position.getY() - frog_position.getY(), 2)) < (HITBOX_WIDTH / 2 + width / 2))
				{
					((Frog*)obj)->set_health(((Frog*)obj)->get_health() - current_sword.get_damage());
				}
				if (((Frog*)obj)->get_health() <= 0)
				{
					((Frog*)obj)->onDead();
				}
				break;
			}
			case MIDDLE_BOSS:
			{
				if (((MiddleBoss*)obj)->get_current_state() == MiddleBossState::DEAD)
				{
					continue;
				}
				Vector middle_boss_position = obj->getPos();
				if (getSeeLeft() && sqrt(powf((position.getX() - width / 2 - HITBOX_WIDTH / 2) - middle_boss_position.getX(), 2)
					+ powf(position.getY() - middle_boss_position.getY(), 2)) < (HITBOX_WIDTH / 2 + ((MiddleBoss*)obj)->get_width() / 2))
				{
					((MiddleBoss*)obj)->set_health(((MiddleBoss*)obj)->get_health() - current_sword.get_damage());
				}
				else if (!getSeeLeft() && sqrt(powf((position.getX() + width / 2 + HITBOX_WIDTH / 2) - middle_boss_position.getX(), 2)
					+ powf(position.getY() - middle_boss_position.getY(), 2)) < (HITBOX_WIDTH / 2 + ((MiddleBoss*)obj)->get_width() / 2))
				{
					((MiddleBoss*)obj)->set_health(((MiddleBoss*)obj)->get_health() - current_sword.get_damage());
				}
				if (((MiddleBoss*)obj)->get_health() <= 0)
				{
					((MiddleBoss*)obj)->onDead();
				}
				break;
			}
			case FINAL_BOSS:
			{
				if (((FinalBoss*)obj)->get_current_state() == FinalBossState::DEAD)
				{
					continue;
				}
				Vector final_boss_position = obj->getPos();
				if (getSeeLeft() && sqrt(powf((position.getX() - width / 2 - HITBOX_WIDTH / 2) - final_boss_position.getX(), 2)
					+ powf(position.getY() - final_boss_position.getY(), 2)) < (HITBOX_WIDTH / 2 + ((FinalBoss*)obj)->get_width() / 2))
				{
					((FinalBoss*)obj)->set_health(((FinalBoss*)obj)->get_health() - current_sword.get_damage());
				}
				else if (!getSeeLeft() && sqrt(powf((position.getX() + width / 2 + HITBOX_WIDTH / 2) - final_boss_position.getX(), 2)
					+ powf(position.getY() - final_boss_position.getY(), 2)) < (HITBOX_WIDTH / 2 + ((FinalBoss*)obj)->get_width() / 2))
				{
					((FinalBoss*)obj)->set_health(((FinalBoss*)obj)->get_health() - current_sword.get_damage());
				}
				if (((FinalBoss*)obj)->get_health() <= 10)
				{
					int get_rand_value = random(50, 150);

					((FinalBoss*)obj)->set_jump_value((float)get_rand_value);
				}
				if (((FinalBoss*)obj)->get_health() <= 0)
				{
					((FinalBoss*)obj)->onDead();
				}
				break;
			}
			default:
				continue;
		}
	}
}

void Player::between_blocks()
{
	for (int i = 0; i < blockPosition.size(); ++i)
	{
		//upper side
		if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y + BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
			can_jump = true;
		}
		//down side
		else if (position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2 && position.getY() < blockPosition[i].y - BLOCK_HEIGHT / 2)
		{
			position.setY(blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2);
			velocity.setXY(0.f, 0.f);
			acceleration.setXY(0.f, 0.f);
		}
		//left side
		else if (position.getX() < blockPosition[i].x - BLOCK_WIDTH / 2 && position.getX() > blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x - BLOCK_WIDTH / 2 - width / 2);
		}
		//right side
		else if (position.getX() > blockPosition[i].x + BLOCK_WIDTH / 2 && position.getX() < blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2 && position.getY() < blockPosition[i].y + BLOCK_HEIGHT / 2 + height / 2 && position.getY() > blockPosition[i].y - BLOCK_HEIGHT / 2 - height / 2)
		{
			position.setX(blockPosition[i].x + BLOCK_WIDTH / 2 + width / 2);
		}
	}

	//upper side
	if (position.getY() >= ((MAP_HEIGHT - 1) * BLOCK_HEIGHT)+25)
	{
		position.setY((MAP_HEIGHT - 1) * BLOCK_HEIGHT+25);
	}
	//down side
	else if(position.getY() <= (BLOCK_HEIGHT* (MAP_HEIGHT-21))+25)
	{
		position.setY(BLOCK_HEIGHT * (MAP_HEIGHT - 21)+25);
	}
	//left side
	else if (position.getX() <= (BLOCK_WIDTH * (MAP_WIDTH - 43))-5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 43)-5);
	}
	//right side
	else if (position.getX() >= (BLOCK_WIDTH * (MAP_WIDTH - 1))-5)
	{
		position.setX(BLOCK_WIDTH * (MAP_WIDTH - 1)-5);
	}
}

void Player::addForce(const Vector& force)
{
	acceleration += force.scale(1.f / mass);
}

void Player::onDead()
{
	gameState = STATE::MAIN_MENU;
	Player* new_player = new Player(Vector(0, 0));
	player = new_player;
	ui.reset();
}

float Player::getHealth() const
{
	return health;
}

void Player::setHealth(float Health)
{
	this->health = Health;
}

void Player::setCanJump(bool canJump)
{
	can_jump = canJump;
}

bool Player::getCanJump()
{
	return can_jump;
}

void Player::setIsLeft(bool isLeft)
{
	is_left = isLeft;
}

bool Player::getIsLeft()
{
	return is_left;
}

void Player::setIsRight(bool isRight)
{
	is_right = isRight;
}

bool Player::getIsRight()
{
	return is_right;
}

void Player::setIsJump(bool isJump)
{
	is_jump = isJump;
}

bool Player::getIsJump()
{
	return is_jump;
}

void Player::setSeeLeft(bool seeLeft)
{
	see_left = seeLeft;
}

bool Player::getSeeLeft()
{
	return see_left;
}

void Player::setIsSlash(bool isSlash)
{
	is_slash = isSlash;
}

bool Player::getIsSalsh()
{
	return is_slash;
}

void Player::setCanSlash(bool canSlash)
{
	can_slash = canSlash;
}

bool Player::getCanSlash()
{
	return can_slash;
}

void Player::check_is_cooldown()
{
	check_current_cooldown += DeltaTime;
	if(check_current_cooldown>0.7f)
	{
		can_slash = true;
	}
}

void Player::takeDamage(float amount)
{
	health -= amount * (100.f - currentArmor.getDamageReductionPercentage()) / 100.f;
	if(health<=0)
	{
		onDead();
	}
}

int Player::get_sword_damage()
{
	return current_sword.get_damage();
}

void Player::getSword(Sword sword)
{
	if (current_sword.get_damage() < sword.get_damage())
	{
		current_sword = sword;
	}
}

void Player::getArmor(Armor armor)
{
	if (currentArmor.getTier() < armor.getTier())
	{
		currentArmor = armor;
	}
	getArmorColor();
}

void Player::getArmor(unsigned char tier)
{
	if (currentArmor.getTier() < tier)
	{
		currentArmor = Armor(tier);
	}
	getArmorColor();
}

void Player::getArmorColor()
{
	if(currentArmor.getTier() == 1)
	{
		armor_color = 0xffffffff;
	}
	else if(currentArmor.getTier() == 2)
	{
		armor_color = 0x0000ffff;
	}
	else if (currentArmor.getTier() == 3)
	{
		armor_color = 0xff00ffff;
	}
	else if (currentArmor.getTier() == 4)
	{
		armor_color = 0xffff00ff;
	}
}

void Player::get_gameCleared(bool gameCleared)
{
	game_cleared = gameCleared;
}

float Player::getJumpValue() const
{
	return jump_value;
}

void Player::setJumpValue(float jumpValue)
{
	jump_value = jumpValue;
}

float Player::getSpeed() const
{
	return speed;
}

void Player::setSpeed(float Speed)
{
	this->speed = Speed;
}



